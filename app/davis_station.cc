#include <ic.h>
#include <cpu.h>
#include <machine.h>
#include <alarm.h>
#include <adc.h>
//#include <mach/mc13224v/spi.h>
#include <arch/arm7/tsc.h>
#include <utility/handler.h>
#include <mach/mc13224v/buck_regulator.h>
#include <battery.h>
#include <rtc.h>
#include "ieee1588.h"
#include "ieee1588_ptp.h"

#include <string.h>

__USING_SYS

typedef Machine::IO IO; //External Interruptions KBI4/KBI5

//melhorar com enum //tipo de mensagem
int mensagem_estacao = 1;

OStream cout; //General

//Threads
Thread * davis;
Thread * sync;
bool interrupt_hecops;
RTC rtc;
bool sent;
long timestamp_last_received;

//PTP
IEEE1588_PTP * _ptp;
IEEE_1588 * _protocol;
bool initialized;

class Header {

public:
	int nodeId;
	unsigned int messageSize;
	int messageType;
	int messageErrors;
	unsigned long timeStamp;
} __attribute__((packed));

class Message_Station {

public:
	Header head;
	int battery;
	int rain;
	int temperature;
	int solarRadiation;
	int uvRadiation;
	int windSpeed;
	int windDirection;
} __attribute__((packed));

Message_Station _message;

int i, j; //Rain Sensor and Wind Speed Sensor
static int iterations = Alarm::INFINITE; //Data Handler
static Alarm::Microsecond time = 10000000; //60 seconds Data Handler - //changeable




IEEE1588_PTP * startPTP()
{
	int i;
	// Iniciando e Configurando o PTP Clock
	_ptp = new IEEE1588_PTP();
	_ptp->clock_stratum = 255;
	kout << "Iniciado com sucesso\n";
	return _ptp;
}

int synchronizeMotes(){
	if(!initialized){
		initialized = true;
		_protocol->startProtocol();
	}else{
		_protocol->synchronizing();
	}
	return 0;
}


//MC13224V_SPI * _spi; //Temperature Sensor - SPI
ADC adc3(ADC::SINGLE_ENDED_ADC3); //Wind Direction Sensor ADC3
float direction; //Wind Direction Sensor
ADC adc4(ADC::SINGLE_ENDED_ADC4); //UV Sensor ADC4
int uvIndex; //UV Sensor
ADC adc5(ADC::SINGLE_ENDED_ADC5); //Solar Sensor ADC5
float solarRadiation; //Solar Sensor
bool data = false;

//WSN Network
NIC * nic;

// Rain and Wind Sensor - Interruption Handler Function KBI4/KBI5
void kbi_handler() {
	//Rain Sensor
	if (CPU::in32(IO::CRM_STATUS) & 0x00000010) { //KBI4
		i = i + 1;
		cout << "KBI4 ";
		cout << i;
		cout << " handled\n";

		CPU::out32(IO::CRM_STATUS, CPU::in32(IO::CRM_STATUS) //Needs to clear flag
		| (1 << 4) //EXT_WU_EVT
		);

		//Wind Speed Sensor
	} else if (CPU::in32(IO::CRM_STATUS) & 0x00000020) { //KBI5
		j = j + 1;
		cout << "KBI5 ";
		cout << j;
		cout << " handled\n";

		CPU::out32(IO::CRM_STATUS, CPU::in32(IO::CRM_STATUS) //Needs to clear flag
		| (2 << 4) //EXT_WU_EVT
		);
	}
}

float getRain(){
	if(i>0){
		return i*0.2f;
	}else
		return 0;
}

//Wind Direction Sensor - ADC3 Reader Function
float getWindDirection() {
	int value = adc3.get();
	direction = (float) ((0.0879 * (value)) - 0.000000000001f);
	return direction;
}

//TO DO
float getWindSpeed(){
	return 0;
}

//Temperature Sensor - SPI Reader Function
int read() {
	//MC13224V_SPI::spiErr_t e;
	unsigned int data[] = { 0 };
//	e = _spi->SPI_ReadSync(data);
	return data[0];
}

float getTemperature(){
	int t = (0x0000FFFF & (read())) >> 3;
	float temperature = (0.0625 * (t));
	return temperature;
}

//Solar Sensor - ADC5 Reader Function
float getSolarRadiation() {
	int value = adc5.get();
	solarRadiation = (float) ((0.4826f * (value)) + 0.000000000002f); // W/mÂ²
	return solarRadiation;
}

//UV Sensor - ADC4 Reader Function
int getUV() {
	int value = adc4.get();
	uvIndex = (int) ((0.0054f * (value)) - 0.000000000000006f);
	return uvIndex;
}
//General - Periodic Amount Data Handler Function - It stores data in some place or it sends via GPRS
void periodic_amount() {

	//TimeStamp - dd/mm/yyyy hh:mm:ss
	//cout << "Hora: " << (tsc.time_stamp() * 1/tsc.frequency()) << endl;
	_message.head.timeStamp = rtc.getTimeStamp();

	//Status Bateria
	//	cout << "Battery status: " << Battery::sys_batt().sample() << " mV" << endl;
	_message.battery = Battery::sys_batt().sample();

	//Rain Sensor - millimeters
	//	cout << "Rain: " << getRain() << endl;
	//cout << "Rain: " << 10 << endl;
	//_message.rain = getRain();
	_message.rain = 10;

	//Temperature Sensor - Celsius
	//cout << "Temperature: " << getTemperature() << endl;
	//cout << "Temperature: " << 25 << endl;
	//_message.temperature = getTemperature();
	_message.temperature = 38;

	//Solar Sensor
	//cout << "Solar Radiation: " << getSolarRadiation() << endl;
	//cout << "Solar Radiation: " << 15 << endl;
	//_message.solarRadiation = getSolarRadiation();
	_message.solarRadiation = 200;

	//UV Sensor
	//cout << "UV Index: " << getUV() << endl;
	//cout << "UV Index: " << 10 << endl;
	//_message.uvRadiation = getUV();
	_message.uvRadiation = 6;

	//Wind Speed Sensor
	//converter isso em m/s ou deixar pro server... //AVG_Speed e MAX_Speed
	//cout << "Wind Speed (Number of interruptions): " << j << endl;
	//cout << "Wind Speed (Number of interruptions): " << 10 << endl;
	//_message.windSpeed = j;
	_message.windSpeed = 5;

	//Wind Direction Sensor  - converter isso em N/S/L/O
	//cout << "Wind Direction: " << getWindDirection() << "Degrees" << endl;
	//cout << "Wind Direction: " << 50 << "Degrees" << endl;
	//_message.windDirection = getWindDirection();
	_message.windDirection = 30;

	i = j = 0;

	data = true;
}

void send_message(){
	if(data){

		_message.head.messageErrors = 0;
		_message.head.messageSize = sizeof(_message);
		_message.head.nodeId = 3;
		_message.head.messageType = mensagem_estacao;

		cout << "\n##########\n";
		cout << "Sender id: "   << (int) _message.head.nodeId << "\n";
		cout << "Msg type: "    << (int) _message.head.messageType << "\n";
		cout << "Msg errors: "  << (int) _message.head.messageErrors << "\n";
		cout << "Msg size: "    << (int) _message.head.messageSize << "\n";
		cout << "Time Stamp: "  << (long) _message.head.timeStamp << "\n";
		cout << "Battery: "     << (int) _message.battery << " mV\n";
		cout << "Temperature: " << (int) _message.temperature << " C\n";
		cout << "Rain: "  << (int) _message.rain << "\n";
		cout << "SolarRad: "    << (int) _message.solarRadiation << "\n";
		cout << "UVRad: "       << (int) _message.uvRadiation << "\n";
		cout << "WindDir: "     << (int) _message.windDirection << "\n";
		cout << "WindSpeed: "   << (int) _message.windSpeed << "\n";

		char * msg;
		msg = (char *)&(_message);

		int ret;
			ret = nic->send(NIC::BROADCAST, (NIC::Protocol) 1, msg, sizeof(Message_Station));
			sent = true;
		cout << "OK: " << ret << endl;
		free(msg);
		data = false;
	}

}

int triDAVIS(){
	while(true){
		if(!interrupt_hecops){
			send_message();
		}else{
			davis->yield();
		}
	}
	return 0;
}

int syncMotes(){
	_protocol->setInterrupt(false);
	while(true){
		if(interrupt_hecops){
			cout << "Synchronizing\n";
			synchronizeMotes();
		}
		else{
			sync->yield();
		}
	}
	return 0;
}

void interrupt(){
	if(interrupt_hecops){
		interrupt_hecops = false;
		_protocol->setInterrupt(true);
	}
	else{
//		if(sent){
		interrupt_hecops = true;
		_protocol->setInterrupt(false);
//		}
	}
}

int main() {
	//General - Periodic Handler Config

	//Rain & Wind Sensor - KBI Interruption Handler Config - KBI4 & KBI5
	IC::int_vector(IC::IRQ_CRM, kbi_handler);
	IC::enable(IC::IRQ_CRM);

	//KBI Interruption Config - KBI4/KBI5
	CPU::out32(IO::GPIO_PAD_DIR0, CPU::in32(IO::GPIO_PAD_DIR0) & ~(3 << 26) // GPIO_26 and GPIO_27 as input
	);
	CPU::out32(IO::GPIO_FUNC_SEL1, CPU::in32(IO::GPIO_FUNC_SEL1) & ~(15 << 20) // GPIO_26 and GPIO_27 are at default function
	);
	CPU::out32(IO::GPIO_PAD_PU_SEL0,
			CPU::in32(IO::GPIO_PAD_PU_SEL0) & ~(1 << 27) // GPIO_27 as weak pull-up
	);
	CPU::out32(IO::GPIO_DATA_SEL0, CPU::in32(IO::GPIO_DATA_SEL0) & ~(3 << 26) // GPIO_26 and GPIO_27 data comes from the pad
	);
	CPU::out32(IO::CRM_WU_CNTL, CPU::in32(IO::CRM_WU_CNTL) | (0x3 << 4) // EXT_WU_EN [3:0]0x4 << 4
			| (0x3 << 8)  // EXT_WU_EDGE 0x4 << 8
			| (0x3 << 12) // EXT_WU_POL 0x4 << 12
			| (0x3 << 20) // EXT_WU_IEN 0x4 << 20
	);

	CPU::int_enable();

	//Temperature Sensor - SPI Configuration
	//MC13224V_SPI::spiConfig_t config;
	//MC13224V_SPI::spiDelay delay = //MC13224V_SPI::delayZero;
	//MC13224V_SPI::spiSdoInactive doInactive = //MC13224V_SPI::Mosi_out_00;
	//MC13224V_SPI::spiClockPol clkPol = //MC13224V_SPI::polOne;
	//MC13224V_SPI::spiClockPhase clkPha = //MC13224V_SPI::phaOne;
	//MC13224V_SPI::spiMisoPhase misoPhase = //MC13224V_SPI::Same;
	//MC13224V_SPI::spiClockFreq clkFreq = //MC13224V_SPI::_1_5MHz;
	//MC13224V_SPI::spi3Wire wire = //MC13224V_SPI::Wire4;
	//MC13224V_SPI::spiMode mode = //MC13224V_SPI::Master;
	//MC13224V_SPI::spiSlaveSelectSetup ssSetup = //MC13224V_SPI::Auto_Active_Low;

	/*config.Setup.Bits.SsSetup = ssSetup; //table 15-10 automatic, low
	config.Setup.Bits.SsDelay = delay;
	config.Setup.Bits.SdoInactive = doInactive;
	config.Setup.Bits.ClockPol = clkPol; //ok CPOL
	config.Setup.Bits.ClockPhase = clkPha; //ok CPHA
	config.Setup.Bits.MisoPhase = misoPhase; //UPDATE ON RISE???
	config.Setup.Bits.ClockFreq = clkFreq; //24/n
	config.Setup.Bits.Mode = mode;  //low = masterMode
	config.Setup.Bits.S3Wire = wire; //4Wire
	config.ClkCtrl.Bits.DataCount = 16; //0b011000;//32-8;
	config.ClkCtrl.Bits.ClockCount = 16; //0b0011000;
*/
	//_spi = new //MC13224V_SPI(config);

	//MSG
//	_message.head.nodeId = 3;
//	_message.head.messageType = mensagem_estacao;

	//WSN Network
	nic = new NIC();
	_ptp = startPTP();
	_protocol = new IEEE_1588(_ptp);
	_protocol->setNIC(nic);
	_protocol->initializeServo();

	data = true;
	Function_Handler handler(&periodic_amount);
	Alarm alarm(time, &handler, iterations);

	Function_Handler handler_ptp(&interrupt);
	Alarm alarm_ptp(33000000, &handler_ptp, -1);

	davis = new Thread(&triDAVIS);
	sync = new Thread(&syncMotes);
	sync->join();

	while(true);
//	while(true){
//	send_message();
//	}
	return 0;
}
