#include <utility/ostream.h>
#include <display.h>
#include <uart.h>
#include <adc.h>
#include <cpu.h>
#include <machine.h>
#include <alarm.h>
#include <utility/string.h>
#include "rtc.h"
#include <utility/handler.h>
#include <hecops.h>
#include <utility/ostream.h>
#include <alarm.h>
#include "ieee1588.h"
#include "ieee1588_ptp.h"
#include "thread.h"
#include "utility/random.h"
#include "radio.h"
#include <uart.h>
#include <rtc.h>
//#include <mach/atmega1281/ic.h>
__USING_SYS;

OStream cout;
UART uart(9600, 8 , 0 , 1);
char msg[100];
char utc[20];
char data[20];
char * date;
RTC _rtc;
int offsetCorrection;
long time_h,time_m,time_s, data_d, data_m, data_a;
long time_follow;
long long _timestamp;

bool isLeap(long a){
	if((a>0) && !(a%4) && ( (a%100) || !(a%400)))
		return true;
	return false;

}


long long get_fev(long a){
	if(isLeap(a))
		return 29;
	return 28;
}

long long get_mes_sec(long m, long a){
	if(m == 2)
		return get_fev(a);
	else{
		if((m <=7 && m%2 == 1) || (m >=8 && m%2==0))
			return 31;
		if((m <=6 && m%2 == 0 ) || (m >=9 && m%2 == 1))
			return 30;
	}
}



void get_epoch()
{

	char hora[3];
	char min[3];
	char sec[3];

	char dia[3];
	char mes[3];
	char ano[3];


	for(int i =0; i< 2 ; i++)
		dia[i] = data[i];

	dia[2] = '\0';

	for(int i =0; i< 2 ; i++)
		mes[i] = data[i+2];

	mes[2] = '\0';

	for(int i =0; i< 2 ; i++)
		ano[i] = data[i+4];

	ano[2] = '\0';

	for(int i =0; i< 2 ; i++)
		hora[i] = utc[i];

	hora[2] = '\0';

	for(int i =0; i< 2 ; i++)
		min[i] = utc[i+2];

	min[2] = '\0';

	for(int i =0; i< 2 ; i++)
		sec[i] = utc[i+4];

	sec[2] = '\0';

	char follow[4];

	for(int i =0;i<3;i++)
		follow[i] = utc[i+7];

	follow[3] = '\0';

	time_h = atol(hora);
	time_m = atol(min);
	time_s = atol(sec);

	data_d = atol(dia);
	data_m = atol(mes);
	data_a = atol(ano);

	time_follow = atol(follow);

	_timestamp = (( data_a + 100 - 70) * 365 * 24 *60 * 60);
	data_a += 2000;
	int count_leaps = ((data_a-1)/4 - (data_a-1)/100 + (data_a-1)/400);
	data_a = 1970;
	count_leaps -= ((data_a-1)/4 - (data_a-1)/100 + (data_a-1)/400);
	_timestamp += count_leaps*24*60*60;


	long long ano_mes = 0;

	for(int i =1;i<data_m;i++){
		ano_mes += get_mes_sec(i, data_a);
	}

	_timestamp += ano_mes * 24 * 60 * 60;

	_timestamp += data_d * 24 * 60 * 60;

	_timestamp += time_h * 60 * 60;

	_timestamp += time_m *60;

	_timestamp += time_s;

	kout << "Time Stamp " << timestamp << "\n";

}

void parse(){

	cout << "PPS\n";

}

void getNmea(){
	cout << "getNmea\n";

	int i;
	while(true){
		i=0;
		if(uart.get() == '\n'){
			while(i < 70){
				msg[i] = uart.get();
				i++;
			}

			if(msg[0] == '$' && msg[1] == 'G' && msg[2] =='P' && msg[3] == 'R'){
				date = _rtc.parseNMEA(msg);

				for(int i=0;i<10;i++)
					utc[i] = date[i];

				for(int i=0;i<17;i++)
					data[i] = date[i+11];

				cout << date << "\n";
				get_epoch();
				cout << "Timestamp Calculado\n";
				free(date);
				break;
			}
		}
	}

}


void ppsHandler(){
	cout << "PPS Handler" << "\n";
}

int main()
{
	//cout << "reiniciando\n";
	//    typedef IO_Map<Machine> IO;
	//   IC::int_vector(IC::IRQ_IRQ0, parse);
	//   CPU::out8(IO::PCMSK0, 0);
	//   CPU::out8(IO::PCICR, 0);
	//   CPU::out8(IO::EICRA, 3);
	//   CPU::out8(IO::EIMSK, (1 << 0));

	cout << "Criando Alarm\n";

	Function_Handler handler(&getNmea);
	Alarm alarm_nmea(5000000, &handler, -1);

	while(true);

	return 0;
}

