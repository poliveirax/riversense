// EPOS HECOPS TestLI Program
#include <hecops.h>
#include <utility/ostream.h>
#include <alarm.h>
#include "thread.h"
#include "radio.h"
#include "periodic_thread.h"

__USING_SYS


NIC * nic;
OStream cout;
NIC::Protocol prot;
NIC::Address src;


class MsgSCADA {

public:
	int msgId;
	int nodeId;
	long x_value;
	long y_value;
	long rssi_value;
	int number_of_sensors;
	unsigned long timeStamp;
} __attribute__((packed));

MsgSCADA * dat;

void receiveAndSendDataToSCADA(){

	char receive_one[nic->mtu()];
	int ret;
	while(true){
		ret = nic->receive(&src, &prot, receive_one, sizeof(MsgSCADA));
		if(ret > 0){
			dat = (MsgSCADA*)receive_one;
			if(dat->msgId == 99)
				kout<< dat->nodeId << ";" << dat->x_value << ";" << dat->y_value << ";" << dat->rssi_value << ";" << dat->number_of_sensors << ";" << dat->timeStamp << endl;
		}
		free(dat);
	}
}
int main(){
	nic = new NIC();
	receiveAndSendDataToSCADA();
}
