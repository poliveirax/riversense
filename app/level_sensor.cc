#include <machine.h>
#include <alarm.h>
#include <sensor.h>
#include <battery.h>
#include <utility/string.h>
#include "ieee1588.h"
#include "ieee1588_ptp.h"
#include <rtc.h>

__USING_SYS

typedef Machine::IO IO;

OStream cout;
UART uart(9600, 8 , 0 , 1);
RTC rtc;

//PTP
IEEE1588_PTP * _ptp;
IEEE_1588 * _protocol;
bool initialized;
Thread * gprs;
Thread * sync;
bool interrupt_gprs;
NIC::Protocol prot;
NIC::Address src;
bool flag;


static int iterations = Alarm::INFINITE;
static Alarm::Microsecond time = 5000000; //30 seconds
volatile unsigned int data0_mask = 0x0;

class Header {

public:
	int nodeId;
	unsigned int messageSize;
	int messageType;
	int messageErrors;
	unsigned long timeStamp;
} __attribute__((packed));

class Message_RiverLevel {

public:
	Header head;
	int battery;
	int riverLevel;
} __attribute__((packed));

Message_RiverLevel _message;

NIC * nic;

void turn_sensor_on() {
	data0_mask |= (1 << 25);
	CPU::out32(IO::GPIO_DATA0, data0_mask);
}

void turn_sensor_off() {
	data0_mask &= ~(1 << 25);
	CPU::out32(IO::GPIO_DATA0, data0_mask);
}

float convert_level(int value) {
	return (float) ((0.0624f * (value)) - 71.79f);
}

//Periodic Amount Data Handler Function
void periodic_read() {
	cout << "Enable Sensor...\n";
	turn_sensor_on();
	flag = true;
}

void read_level() {
	int adcValue;
	//cout << "ADC Value= ";
	//adcValue = adc.get();
	//cout << adcValue << endl;
	//cout << "Level: " << convert_level(adc.get()) << endl;
	cout << "Disable Sensor...\n";

	//convert_level(adc.get());
	//turn_sensor_off();

}

int main() {
	//Periodic Handler Config
	Function_Handler handler(&periodic_read);
	Alarm alarm(time, &handler, iterations);

	//configurar KBI3 para output
	/*CPU::out32(IO::GPIO_PAD_DIR0, 0u // all as input, but:
	| (1 << 25) // GPIO_25: Saída
			);

	CPU::int_enable();

	turn_sensor_off();
	 */
	while (true) {

		cout << "Waiting...\n";
		//Alarm::delay(1000000);

		if (flag) {
			//			Alarm::delay(10000);
			//read_level();
			//_message.riverLevel = convert_level(adc.get());

			char * msg;
			msg = (char *)&(_message);
			int r;
			_message.head.messageErrors = 0;

			cout << "\n##########\n";
			cout << "Sender id: "   << (int) _message.head.nodeId << "\n";
			cout << "Msg errors: "  << (int) _message.head.messageErrors << "\n";
			cout << "Battery: "     << (int) _message.battery << " \mV\n";
			cout << "River Level: " << (int) _message.riverLevel << " m\n";


			while ((r = nic->send(NIC::BROADCAST, (NIC::Protocol) 1, msg, sizeof(Message_RiverLevel))) != 11) {
				//_message.head.messageErrors++;
				cout << "failed " << r << "\n";
				//msg = (char *)&(_message);
			}
			cout << "OK: " << r << endl;
			free(msg);

			flag = false;
		}
	}

	return 0;
}

