#include <ic.h>
#include <cpu.h>
#include <machine.h>
#include <alarm.h>
#include <adc.h>
#include <arch/arm7/tsc.h>
#include <utility/handler.h>
#include <mach/mc13224v/buck_regulator.h>
#include <battery.h>
#include <rtc.h>
#include "ieee1588.h"
#include "ieee1588_ptp.h"

#include <string.h>

__USING_SYS

typedef Machine::IO IO;

OStream cout;
ADC adc(ADC::SINGLE_ENDED_ADC2);
bool flag = false;
static int iterations = Alarm::INFINITE; //Data Handler
static Alarm::Microsecond time = 10000000; //50 seconds Data Handler - //changeable
volatile unsigned int data0_mask = 0x0;
bool sent;
long timestamp_last_received;
RTC rtc;
//Threads
Thread * davis;
Thread * sync;
bool interrupt_hecops;

//PTP
IEEE1588_PTP * _ptp;
IEEE_1588 * _protocol;
bool initialized;


//melhorar com enum //tipo de mensagem
int mensagem_RiverLivel = 2;

void turn_sensor_on() {
	data0_mask |= (1 << 25);
	CPU::out32(IO::GPIO_DATA0, data0_mask);
}

void turn_sensor_off() {
	data0_mask &= ~(1 << 25);
	CPU::out32(IO::GPIO_DATA0, data0_mask);
}

//Periodic Amount Data Handler Function
void periodic_read() {
	if(rtc.getTimeStamp() - timestamp_last_received > 120){
		cout << "Enable Sensor...\n";
		//turn_sensor_on();
	}
	flag = true;

}

float convert_level(int value) {
	float level = (float) ((0.0624f * (value)) - 71.79f);
	turn_sensor_off();
	return level;
}


class Header {

public:
	int nodeId;
	unsigned int messageSize;
	int messageType;
	int messageErrors;
	unsigned long timeStamp;
} __attribute__((packed));

class Message_RiverLevel {

public:
	Header head;
	int battery;
	float riverLevel;
} __attribute__((packed));

Message_RiverLevel _message;


IEEE1588_PTP * startPTP()
{
	int i;
	// Iniciando e Configurando o PTP Clock
	_ptp = new IEEE1588_PTP();
	_ptp->clock_stratum = 100;
	kout << "Iniciado com sucesso\n";
	return _ptp;
}

int synchronizeMotes(){
	if(!initialized){
		initialized = true;
		_protocol->startProtocol();
	}else{
		_protocol->synchronizing();
	}
	return 0;
}


//WSN Network
NIC * nic;


void send_message(){
	if(flag){
		flag = false;
		//Alarm::delay(100000); //100ms
		//cout << "Nivel: " << convert_level(adc.get()) << endl;
		//_message.riverLevel = convert_level(adc.get());
		_message.riverLevel = 15;

		_message.head.timeStamp = rtc.getTimeStamp();
		_message.battery = Battery::sys_batt().sample();
		_message.head.messageErrors = 0;
		_message.head.messageSize = sizeof(_message);

		cout << "\n##########\n";
		cout << "Sender id: "  << (int) _message.head.nodeId << "\n";
		cout << "Msg type: "   << (int) _message.head.messageType << "\n";
		cout << "Msg errors: " << (int) _message.head.messageErrors << "\n";
		cout << "Msg size: "   << (int) _message.head.messageSize << "\n";
		cout << "Time Stamp: " << (long) _message.head.timeStamp << "\n";
		cout << "Battery: "    << (int) _message.battery << " mV\n";
		cout << "Nivel: "      << (float) _message.riverLevel << " cm\n";

		char * msg;
		msg = (char *)&(_message);

		int ret;
		for(int i=0;i < 2; i++){
			ret = nic->send(NIC::BROADCAST, (NIC::Protocol) 1, msg, sizeof(Message_RiverLevel));
		}
		if(ret == 11)
			sent = true;
		cout << "OK: " << ret << endl;
		free(msg);
	}

}

int triDAVIS(){
	while(true){
		if(!interrupt_hecops){
			send_message();
		}else{
			davis->yield();
		}
	}
	return 0;
}

int syncMotes(){
	_protocol->setInterrupt(false);
	while(true){
		if(interrupt_hecops){
			cout << "Synchronizing\n";
			synchronizeMotes();
		}
		else{
			sync->yield();
		}
	}
	return 0;
}

void interrupt(){
	if(interrupt_hecops){
		interrupt_hecops = false;
		_protocol->setInterrupt(true);
	}
	else{
		if(sent){
			interrupt_hecops = true;
			_protocol->setInterrupt(false);
		}
	}
}

int main() {
	//Periodic Handler Config
	Function_Handler handler(&periodic_read);
	Alarm alarm(time, &handler, iterations);
	/*
	//configurar KBI3 para output
	CPU::out32(IO::GPIO_PAD_DIR0, 0u // all as input, but:
	| (1 << 25) // GPIO_25: Saída
	);

	CPU::int_enable();

	turn_sensor_off();
	 */
	//MSG
	_message.head.nodeId = 2;
	_message.head.messageType = mensagem_RiverLivel;

	//WSN Network
	nic = new NIC();
	_ptp = startPTP();
	_protocol = new IEEE_1588(_ptp);
	_protocol->setNIC(nic);
	_protocol->initializeServo();

	flag = true;

	Function_Handler handler_ptp(&interrupt);
	Alarm alarm_ptp(33000000, &handler_ptp, -1);

	davis = new Thread(&triDAVIS);
	sync = new Thread(&syncMotes);
	sync->join();

	while(true);
	//	while(true){
	//		send_message();
	//	}
	return 0;
}
