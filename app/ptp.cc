
#include "ieee1588.h"
#include "ieee1588_ptp.h"
#include <rtc.h>
#include <uart.h>
#include <utility/ostream.h>
#include <thread.h>

__USING_SYS;

OStream cout;
NIC * _nic;
IEEE1588_PTP * _ptp;
IEEE_1588 * _protocol;
bool initialized;
RTC rtc;
Thread * sync;
UART uart(9600, 8 , 0 , 1);
unsigned long _timestamp;

IEEE1588_PTP * startPTP()
{
	_ptp = new IEEE1588_PTP();
	_ptp->sync_interval = 15000000;
	_ptp->clock_stratum = 4;
	_ptp->initialized = 0;
	return _ptp;
}

int synchronizeMotes(){
	cout << "Synchronizing with PTP\n";
	if(!initialized){
		initialized = true;
		_protocol->startProtocol();
	}
	return 0;
}

bool isLeap(long a){
	if((a>0) && !(a%4) && ( (a%100) || !(a%400)))
		return true;
	return false;

}

long long get_fev(long a){
	if(isLeap(a))
		return 29;
	return 28;
}

long long get_mes_sec(long m, long a){
	if(m == 2)
		return get_fev(a);
	else{
		if((m <=7 && m%2 == 1) || (m >=8 && m%2==0))
			return 31;
		if((m <=6 && m%2 == 0 ) || (m >=9 && m%2 == 1))
			return 30;
	}
}

void get_epoch(char * utc, char * data)
{
	int offsetCorrection;
	long time_h,time_m,time_s, data_d, data_m, data_a;
	long time_follow;
	char hora[3];
	char min[3];
	char sec[3];
	char dia[3];
	char mes[3];
	char ano[3];
	for(int i =0; i< 2 ; i++)
		dia[i] = data[i];

	dia[2] = '\0';

	for(int i =0; i< 2 ; i++)
		mes[i] = data[i+2];

	mes[2] = '\0';

	for(int i =0; i< 2 ; i++)
		ano[i] = data[i+4];

	ano[2] = '\0';

	for(int i =0; i< 2 ; i++)
		hora[i] = utc[i];

	hora[2] = '\0';

	for(int i =0; i< 2 ; i++)
		min[i] = utc[i+2];

	min[2] = '\0';

	for(int i =0; i< 2 ; i++)
		sec[i] = utc[i+4];

	sec[2] = '\0';

	char follow[4];

	for(int i =0;i<3;i++)
		follow[i] = utc[i+7];

	follow[3] = '\0';

	time_h = atol(hora);
	time_m = atol(min);
	time_s = atol(sec);

	data_d = atol(dia);
	data_m = atol(mes);
	data_a = atol(ano);

	time_follow = atol(follow);

	_timestamp = (( data_a + 100 - 70) * 365 * 24 *60 * 60);
	data_a += 2000;
	int count_leaps = ((data_a-1)/4 - (data_a-1)/100 + (data_a-1)/400);
	data_a = 1970;
	count_leaps -= ((data_a-1)/4 - (data_a-1)/100 + (data_a-1)/400);
	_timestamp += count_leaps*24*60*60;


	long long ano_mes = 0;

	for(int i =1;i<data_m;i++){
		ano_mes += get_mes_sec(i, data_a);
	}

	_timestamp += ano_mes * 24 * 60 * 60;

	_timestamp += data_d * 24 * 60 * 60;

	_timestamp += time_h * 60 * 60;

	_timestamp += time_m *60;

	_timestamp += time_s;

}

void updateClock(){
	int i;
	char msg[100];
	char utc[20];
	char data[20];
	char * date;
	for(int j=0;j<1000;j++){
		uart.get();
		i=0;
		if(uart.get() == '\n'){
			while(i < 70){
				msg[i] = uart.get();
				i++;
			}
			if(msg[0] == '$' && msg[1] == 'G' && msg[2] =='P' && msg[3] == 'R'){
				date = rtc.parseNMEA(msg);
				for(int i=0;i<10;i++)
					utc[i] = date[i];
				for(int i=0;i<17;i++)
					data[i] = date[i+11];
				get_epoch(utc,data);
				free(date);
				break;
			}
		}
	}
	rtc.setTimeStamp(timestamp);
	cout << rtc.getTimeStamp() << "\n";
}


void initialize_sync(){
	_ptp->initialized = false;
	_protocol->setInterrupt(false);
	synchronizeMotes();
}


void initialize_clock(){
	_ptp->initialized = false;
	_protocol->setInterrupt(true);
	updateClock();
	initialize_sync();
}

void shutdown(IEEE1588_PTP * _ptp)
{
	cout << "DESLIGANDO\n";
}


int main(int argc, char **argv)
{
	RTC::Date d(0,0, 0, 0, 0, 10);
	rtc.date(d);
	rtc.setTimeStamp(rtc.seconds_since_epoch());

	_nic = new NIC();
	_ptp = startPTP();
	_protocol = new IEEE_1588(_ptp);
	_protocol->setNIC(_nic);
	_protocol->initializeServo();

	Function_Handler handler_b(&initialize_clock);
	Alarm alarm_b(63000000, &handler_b, -1);
	initialize_sync();
	while(true);
	return 1;
}
