
#include "ieee1588.h"
#include "ieee1588_ptp.h"
#include <utility/ostream.h>

__USING_SYS;

OStream cout;
IEEE1588_PTP * _ptp;
IEEE_1588 * _protocol;


/** Fun��o Principal para iniciar o aplicativo PTP para sincronizar os rel�gios */

IEEE1588_PTP * startPTP()
{
    int i;
    //struct timespec ts;

// Iniciando e Configurando o PTP Clock
    _ptp = new IEEE1588_PTP();

    _ptp->sync_interval = 1000000;
    _ptp->announceInterval = 2000000;  // AKB: Added for V2
    _ptp->clock_stratum = 100;
    _ptp->inboundLatency.nanoseconds  = 0;
	_ptp->outboundLatency.nanoseconds = 0;
	_ptp->s = 6;
	_ptp->ap = 10;
	_ptp->ai = 1000;
	_ptp->currentUtcOffset = 33;


cout << "Iniciando o PTP \n" ;


	// At� aqui todos os testes iniciais foram feitos e configuramos os parametros do ptp

  cout << "Iniciado com sucesso\n";
  return _ptp;
}

void shutdown(IEEE1588_PTP * _ptp)
{
	cout << "DESLIGANDO\n";
  //netShutdown(&ptpClock->netPath); // network shutdown
//destruit PTP Clock

}


int main(int argc, char **argv)
{
	_protocol = new IEEE_1588();
  int ret;

	 if( !(_ptp = startPTP()) )
		 return ret;

  // Verifica se a op��o probe est� habilitada ( envia uma mensagem de management e depois sai)
  // Caso n�o esteja, segue em frente e inicia o codigo do PTP

  if(_ptp->probe)
  {
    //probe(_ptp);
    shutdown(_ptp);
    return 1;
  }
  else
    	  _protocol->startProtocol(_ptp); //loop infinito na maquina de estados do protocolo

  shutdown(_ptp); // finaliza a execu��o do app limpando a mem�ria criada pelo pr�prio

  return 1;
}





