
#include "ieee1588.h"
#include "ieee1588_ptp.h"
#include <utility/ostream.h>

__USING_SYS;

IEEE1588_PTP * _ptp;
IEEE_1588 * _protocol;
NIC * _nic;

IEEE1588_PTP * startPTP()
{
	int i;
	// Iniciando e Configurando o PTP Clock
	_ptp = new IEEE1588_PTP();
	_ptp->clock_stratum = 255;
	_ptp->inboundLatency.nanoseconds  = 0;
	_ptp->outboundLatency.nanoseconds = 0;
	_ptp->currentUtcOffset = 33;
	kout << "Iniciado com sucesso\n";
	return _ptp;
}

void shutdown(IEEE1588_PTP * _ptp)
{
	kout << "DESLIGANDO\n";
}


int main(int argc, char **argv)
{
	_nic = new NIC();
	_ptp = startPTP();
	_protocol = new IEEE_1588(_ptp);
	_protocol->setNIC(_nic);
	_protocol->initializeServo();
	_protocol->setInterrupt(false);
	_protocol->startProtocol();
	while(true);
	return 1;
}




