#include <machine.h>
#include <alarm.h>
#include <sensor.h>
#include <battery.h>
#include <utility/string.h>
#include "ieee1588.h"
#include "ieee1588_ptp.h"
#include <rtc.h>
#include <string.h>
#include <chronometer.h>

__USING_SYS

OStream cout;
NIC * nic;
UART uart(9600, 8 , 0 , 1);
RTC rtc;
Chronometer chron;

//PTP
IEEE1588_PTP * _ptp;
IEEE_1588 * _protocol;
bool initialized;
Thread * gprs;
Thread * sync;
bool interrupt_gprs;
NIC::Protocol prot;
NIC::Address src;

const unsigned char SINK_ID = 0x01;
int _messageType;
char * date;
char msg[100];
bool dateRequestFlag = false;
bool received_davis, received_level;

class Header {

public:
	int nodeId;
	unsigned int messageSize;
	int messageType;
	int messageErrors;
	unsigned long timeStamp;
} __attribute__((packed));

class Message_Station {

public:
	Header head;
	int battery;
	int rain;
	int temperature;
	int solarRadiation;
	int uvRadiation;
	int windSpeed;
	int windDirection;
} __attribute__((packed));

class Message_RiverLevel {

public:
	Header head;
	int battery;
	float riverLevel;
} __attribute__((packed));

Header * _header;
Message_Station * message;
Message_RiverLevel * message_river;

IEEE1588_PTP * startPTP()
{
	_ptp = new IEEE1588_PTP();
	_ptp->sync_interval = 11000000; //microsecond
	_ptp->clock_stratum = 4; //Master
	_ptp->_state = IEEE_1588::PTP_MASTER;
	_ptp->initialized = 0;
	return _ptp;

}

//GPRS - UART send function
void send(const char * s){
	while(*s != '\0')
		uart.put(*s++);
}

void dateRequest(){
	dateRequestFlag = true;
}

int sink() {

	//Alarm::delay(10000000); //10s

	//	cout << "Gateway Node\n";
	//	cout << rtc.getTimeStamp() << "\n\n";

	/*if(dateRequestFlag == true){
		dateRequestFlag = false;
		uart.put('b');
		chron.start();
		//Alarm::delay(20000000); //2s
		int i;

		//D1359140533
		//char comando = uart.get();
		if(uart.get() == 'D'){
			i=0;
			while(i < 10){
				msg[i] = uart.get();
				i++;
			}
			int timestamp = atoi(msg);
			cout << "Server TimeStamp: "  << timestamp << endl;
			rtc.setTimeStamp(timestamp);
			chron.stop();
		}
		cout << "TOA: " << chron.read() << " us" << endl;
	}
*/
	//Mensagens

	char data[nic->mtu()];
//	while((nic->receive(&src, &prot, data, sizeof(data))) < 0);
	nic->receive(&src, &prot, data, sizeof(data));

	_header = (Header*) data;

	_messageType = _header->messageType;

	switch (_messageType) {

	case 1: {
		if(!received_davis){
			message = (Message_Station *)data;
			cout << "\n##########\n";
			cout << "Sender id: "   << (int) message->head.nodeId << "\n";
			cout << "Msg errors: "  << (int) message->head.messageErrors << "\n";
			cout << "Msg size: "    << (int) message->head.messageSize << "\n";
			cout << "Time Stamp: "  << (long) message->head.timeStamp << "\n";
			//message->head.timeStamp = rtc.getTimeStamp();
			//cout << "Time Stamp: "  << (long) message->head.timeStamp << "\n";
			cout << "Battery: "     << (int) message->battery << " \%\n";
			cout << "Temperature: " << (int) message->temperature << " C\n";
			cout << "Rain: "        << (int) message->rain << "\n";
			cout << "SolarRad: "    << (int) message->solarRadiation << "\n";
			cout << "UVRad: "       << (int) message->uvRadiation << "\n";
			cout << "WindDir: "     << (int) message->windDirection << "\n";
			cout << "WindSpeed: "   << (int) message->windSpeed << "\n";
			received_davis = true;
//			free(message);
		}
	}break;

	case 2: {
		if(!received_level){
			message_river = (Message_RiverLevel*)data;
			cout << "\n##########\n";
			cout << "Sender id: "  << (int) message_river->head.nodeId << "\n";
			cout << "Msg type: "   << (int) message_river->head.messageType << "\n";
			cout << "Msg errors: " << (int) message_river->head.messageErrors << "\n";
			cout << "Msg size: "   << (int) message_river->head.messageSize << "\n";
			cout << "Time Stamp: " << (long) message_river->head.timeStamp << "\n";
			//message_river->head.timeStamp = rtc.getTimeStamp();
			//cout << "Time Stamp: " << (long) message_river->head.timeStamp << "\n";
			cout << "Battery: "    << (int) message_river->battery << " mV\n";
			cout << "Nivel: "      << (float) message_river->riverLevel << " cm\n";
			received_level = true;
//			free(message_river);
		}
	}break;

	default: {

	}break;

	}
	free(data);
	_messageType = 0;
	free(_header);
	return 0;
}


int synchronizeMotes(){
	if(!initialized){
		initialized = true;
		_protocol->startProtocol();
	}else{
		_protocol->synchronizing();
	}
	return 0;
}

int syncMotes(){
	_protocol->setInterrupt(false);
	if(interrupt_gprs){
		cout << "Synchronizing\n";
		synchronizeMotes();
	}
	else{
		gprs->join();
	}
	return 0;
}

void interrupt(){
	if(interrupt_gprs){
		interrupt_gprs = false;
		received_davis = false;
		received_level = false;
	}
	else{
		if(received_davis == true && received_level == true)
			interrupt_gprs = true;
	}
}

int triGPRS(){
	while(true){
		if(!interrupt_gprs){
			sink();
		}else{
			gprs->yield();
		}
	}
	return 0;
}

int main() {

	kout << "SINK GPRS\n";
	rtc.setTimeStamp(1358140533);

	nic = new NIC();
	//INICIALIZADO PTP
	_ptp = startPTP();
	_protocol = new IEEE_1588(_ptp);
	_protocol->setNIC(nic);
	_protocol->initializeServo();

	dateRequestFlag = true;

	//Function_Handler handler_Date(&dateRequest);
	//Alarm alarm_Date(480000000, &handler_Date, -1); //10 segundos / alterar para 4 minutos

	Function_Handler handler_ptp(&interrupt);
	Alarm alarm_ptp(33000000, &handler_ptp, -1); //33 segundos / alterar para 3 minutos

	gprs = new Thread(&triGPRS);
	synchronizeMotes();

	while(true);
	//	while(true){
	//		sink();
	//	}
	return 0;
}
