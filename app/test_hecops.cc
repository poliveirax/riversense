// EPOS HECOPS TestLI Program

#include <hecops.h>
#include <utility/ostream.h>
#include <alarm.h>
#include "ieee1588.h"
#include "ieee1588_ptp.h"
#include "thread.h"
#include "utility/random.h"
#include "radio.h"
#include <uart.h>
#include <rtc.h>

__USING_SYS


typedef struct{
	int id;
	int porta;
	float valor;
}Mensagem;

RTC rtc;
IEEE1588_PTP * _ptp;
IEEE_1588 * _protocol;
NIC * nic;
OStream cout;
HECOPS * h;
NIC::Protocol prot;
NIC::Address src;
HECOPS::Node node;
HECOPS::Message m;
HECOPS::Message* msg_hecops;
Thread * hecops;
Thread * sync;
int uwb,uwb2,uwb3,uwb4;
Radio_Wrapper r;
bool interrupt_hecops;
bool initialized;
Mensagem scada_msg;
UART uart(9600, 8 , 0 , 1);
unsigned long _timestamp;
bool shouldI;

char * dat;

class MsgSCADA {

public:
	int msgId;
	int nodeId;
	long x_value;
	long y_value;
	long rssi_value;
	int number_of_sensors;
	unsigned long timeStamp;
} __attribute__((packed));

MsgSCADA scadinha;




bool isLeap(long a){
	if((a>0) && !(a%4) && ( (a%100) || !(a%400)))
		return true;
	return false;

}

long long get_fev(long a){
	if(isLeap(a))
		return 29;
	return 28;
}

long long get_mes_sec(long m, long a){
	if(m == 2)
		return get_fev(a);
	else{
		if((m <=7 && m%2 == 1) || (m >=8 && m%2==0))
			return 31;
		if((m <=6 && m%2 == 0 ) || (m >=9 && m%2 == 1))
			return 30;
	}
}

void get_epoch(char * utc, char * data)
{
	int offsetCorrection;
	long time_h,time_m,time_s, data_d, data_m, data_a;
	long time_follow;
	char hora[3];
	char min[3];
	char sec[3];
	char dia[3];
	char mes[3];
	char ano[3];
	for(int i =0; i< 2 ; i++)
		dia[i] = data[i];

	dia[2] = '\0';

	for(int i =0; i< 2 ; i++)
		mes[i] = data[i+2];

	mes[2] = '\0';

	for(int i =0; i< 2 ; i++)
		ano[i] = data[i+4];

	ano[2] = '\0';

	for(int i =0; i< 2 ; i++)
		hora[i] = utc[i];

	hora[2] = '\0';

	for(int i =0; i< 2 ; i++)
		min[i] = utc[i+2];

	min[2] = '\0';

	for(int i =0; i< 2 ; i++)
		sec[i] = utc[i+4];

	sec[2] = '\0';

	char follow[4];

	for(int i =0;i<3;i++)
		follow[i] = utc[i+7];

	follow[3] = '\0';

	time_h = atol(hora);
	time_m = atol(min);
	time_s = atol(sec);

	data_d = atol(dia);
	data_m = atol(mes);
	data_a = atol(ano);

	time_follow = atol(follow);

	_timestamp = (( data_a + 100 - 70) * 365 * 24 *60 * 60);
	data_a += 2000;
	int count_leaps = ((data_a-1)/4 - (data_a-1)/100 + (data_a-1)/400);
	data_a = 1970;
	count_leaps -= ((data_a-1)/4 - (data_a-1)/100 + (data_a-1)/400);
	_timestamp += count_leaps*24*60*60;


	long long ano_mes = 0;

	for(int i =1;i<data_m;i++){
		ano_mes += get_mes_sec(i, data_a);
	}

	_timestamp += ano_mes * 24 * 60 * 60;

	_timestamp += data_d * 24 * 60 * 60;

	_timestamp += time_h * 60 * 60;

	_timestamp += time_m *60;

	_timestamp += time_s;

	kout << "timestamp " << _timestamp <<"\n";

}

void updateClock(){
	int i;
	char msg[100];
	char utc[20];
	char data[20];
	char * date;
	for(int j=0;j<1000;j++){
		uart.get();
		i=0;
		kout << uart.get() << "\n";
		if(uart.get() == '\n'){
			while(i < 70){
				msg[i] = uart.get();
				i++;
			}
			if(msg[0] == '$' && msg[1] == 'G' && msg[2] =='P' && msg[3] == 'R'){
				date = rtc.parseNMEA(msg);
				for(int i=0;i<10;i++)
					utc[i] = date[i];
				for(int i=0;i<17;i++)
					data[i] = date[i+11];
				get_epoch(utc,data);
				free(date);
				break;
			}
		}
	}
	//rtc.setTimeStamp(timestamp);
	cout << "TimeStamp from GPS " << rtc.getTimeStamp() << "\n";
}


int getDataFromUWB(){
	cout << "UWB: "<< uwb <<"\n";
	Pseudo_Random * r = new Pseudo_Random();
	r->seed(3);
	uwb  = r->random()/1000;
	//	char msg[3];
	//	while(true){
	//		if(uart.get() == '\n'){
	//			for(int i=0;i<3;i++)
	//				msg[i] = uart.get();
	//
	//			cout << "MSG " << msg << "\n";
	//		}
	//	}
	return uwb;
}


IEEE1588_PTP * startPTP()
{
	_ptp = new IEEE1588_PTP();
	_ptp->sync_interval = 10000000; //microsecond
	_ptp->clock_stratum = 4; //Master
	_ptp->_state = IEEE_1588::PTP_MASTER;
	_ptp->initialized = 0;
	return _ptp;

}

int synchronizeMotes(){
	if(!initialized){
		initialized = true;
		_protocol->startProtocol();
	}else{
		_protocol->synchronizing();
	}
	return 0;
}


void sendDataToSCADA(){

	char * ms;
	kout << "Sending to SCADABR\n";
	scadinha.nodeId = 3;
	scadinha.msgId = 99;
	scadinha.x_value = node.x;
	scadinha.y_value = node.y;
	scadinha.rssi_value = 0;
	scadinha.number_of_sensors = 1;
	scadinha.timeStamp = rtc.getTimeStamp();
	ms = (char *)&(scadinha);
	nic->send(NIC::BROADCAST, (NIC::Protocol) 1, ms, sizeof(MsgSCADA));
	//DEBUG
	kout << scadinha.nodeId << scadinha.x_value << scadinha.y_value <<	scadinha.rssi_value << scadinha.number_of_sensors << scadinha.timeStamp << endl;
	//free(scadinha);
}


void anchor(char id, long x, long y){
	cout << "id: " << id << "x: " << x << "y: "<< y << "\n";
	m.src = 'X';
	m.dst = HECOPS::EVERYBODY;
	//	m.n = node;
	m.n.x = x;
	m.n.y = y;
	m.n.id = 'X';
	m.n.rssi = 10;
	m.n.id = m.src;
	node.x = x;
	node.y = y;
}



void receive_hecops_message(){
	int ret;
	char * msg_two;
	char data[nic->mtu()];


	ret = nic->receive(&src, &prot, data, sizeof(data));
	if(ret > 0){
		msg_hecops = (HECOPS::Message*)data;
		cout<< "Dado Recebido X : " << msg_hecops->n.x << "\n";
		cout<< "Dado Recebido Y : " << msg_hecops->n.y << "\n";
		//while(ret<0){
		//if(node->is_anchor(msg_hecops->src)){
		//	msg_hecops->n.rssi = msg_hecops->rssi;
		//	HECOPS::Message mg;
		//	mg.src = id;
		//	mg.dst = HECOPS::EVERYBODY;
		//	mg.n = msg_hecops->n;
		//	msg_two = (char *)&(mg);
		//	ret = nic->send(NIC::BROADCAST,(NIC::Protocol) 1,msg_two,sizeof(msg_two));
		//	cout << ret << "\n";
		//}
	}
	//Alarm::delay(1000000);
}

void start_hecops(){
	int ret;
	char * msg;
	//m->n.LQItoRSSI(r.lqi());

	if(shouldI){
		msg = (char *)&(m);
		//	m->n = node;
		cout << "RSSI " << node.id << node.x << node.y << "\n";
		cout << "RSSI " << m.src << m.n.x << m.n.y << "\n";

		ret =	nic->send(NIC::BROADCAST, (NIC::Protocol) 1, msg, sizeof(HECOPS::Message));

		sendDataToSCADA();
		shouldI = false;
	}
	//	cout << ret << "\n";

}

int triHECOPS(){
	while(true){
		if(!interrupt_hecops){
			start_hecops();
		}else{
			hecops->yield();
		}
	}
	return 0;
}

int syncMotes(){
	_protocol->setInterrupt(false);
	if(interrupt_hecops){
		cout << "Synchronizing\n";
		synchronizeMotes();
	}
	else{
		hecops->join();
	}
	return 0;
}

void initialize_uwb(){
	getDataFromUWB();
}

void initialize_scada(){
	sendDataToSCADA();
}

void interrupt(){
	if(interrupt_hecops){
		interrupt_hecops = false;
	}
	else{
		interrupt_hecops = true;
	}
}

void shouldISend(){
	shouldI = true;
}

void initialize_clock(){
	//	_ptp->initialized = false;
	//	_protocol->setInterrupt(true);
	kout << "Updating Clock\n";
	updateClock();
}

int main(){
	rtc.setTimeStamp(1359140533);
	nic = new NIC();
	//INICIALIZADO PTP
	_ptp = startPTP();
	_protocol = new IEEE_1588(_ptp);
	_protocol->setNIC(nic);
	_protocol->initializeServo();
	//INICIALIZANDO HECOPS
	anchor('X', 10, 10);

	Function_Handler handler_b(&shouldISend);
	Alarm alarm_b(10000000, &handler_b, -1);

	Function_Handler handler_ptp(&interrupt);
	Alarm alarm_ptp(33000000, &handler_ptp, -1);

	hecops = new Thread(&triHECOPS);
	synchronizeMotes();

	while(true);
	return 0;

	//	//Alarm para trocar contexto para SCADABR
	//	Function_Handler handler_scada(&initialize_scada);
	//	Alarm alarm_scada(53000000, &handler_scada, -1);

}
