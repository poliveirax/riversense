// EPOS HECOPS TestLI Program

#include <hecops.h>
#include <utility/ostream.h>
#include <alarm.h>
#include "ieee1588.h"
#include "ieee1588_ptp.h"
#include "thread.h"
#include "utility/random.h"
#include "radio.h"
#include <uart.h>
#include <rtc.h>

__USING_SYS


typedef struct{
	int id;
	int porta;
	float valor;
}Mensagem;

RTC rtc;
IEEE1588_PTP * _ptp;
IEEE_1588 * _protocol;
NIC * nic;
OStream cout;
HECOPS * h;
NIC::Protocol prot;
NIC::Address src;
HECOPS::Node node;
HECOPS::Message m;
HECOPS::Message* msg_hecops;
Thread * hecops;
Thread * sync;
int uwb,uwb2,uwb3,uwb4;
Radio_Wrapper r;
bool interrupt_hecops;
bool initialized;
Mensagem scada_msg;
UART uart(9600, 8 , 0 , 1);
unsigned long _timestamp;
char * dat;
bool shouldI;

class MsgSCADA {

public:
	int msgId;
	int nodeId;
	long x_value;
	long y_value;
	long rssi_value;
	int number_of_sensors;
	unsigned long timeStamp;
} __attribute__((packed));


MsgSCADA scadinha;


int getDataFromUWB(){
	cout << "UWB: "<< uwb <<"\n";
	Pseudo_Random * r = new Pseudo_Random();
	r->seed(3);
	uwb  = r->random()/1000;
	//	char msg[3];
	//	while(true){
	//		if(uart.get() == '\n'){
	//			for(int i=0;i<3;i++)
	//				msg[i] = uart.get();
	//
	//			cout << "MSG " << msg << "\n";
	//		}
	//	}
	return uwb;
}

void sendDataToSCADA(){

	char * ms;
	cout << "Sending to SCADABR\n";
	scadinha.nodeId = 1;
	scadinha.msgId = 99;
	scadinha.x_value = node.x;
	scadinha.y_value = node.y;
	scadinha.rssi_value = 0;
	scadinha.number_of_sensors = 1;
	scadinha.timeStamp = rtc.getTimeStamp();

	ms = (char *)&(scadinha);
	nic->send(NIC::BROADCAST, (NIC::Protocol) 1, ms, sizeof(MsgSCADA));
	//DEBUG
	kout << scadinha.nodeId << scadinha.x_value << scadinha.y_value <<	scadinha.rssi_value << scadinha.number_of_sensors << scadinha.timeStamp << endl;
}

IEEE1588_PTP * startPTP()
{
	_ptp = new IEEE1588_PTP();
	_ptp->sync_interval = 10000000; //microsecond
	_ptp->clock_stratum = 100; //Listener
	_ptp->_state = IEEE_1588::PTP_MASTER;
	_ptp->initialized = 0;
	return _ptp;

}

int synchronizeMotes(){
	if(!initialized){
		initialized = true;
		_protocol->startProtocol();
	}else{
		_protocol->synchronizing();
	}
	return 0;
}

void anchor(char id, long x, long y){
	cout << "id: " << id << "x: " << x << "y: "<< y << "\n";
	m.src = 'Y';
	m.dst = HECOPS::EVERYBODY;
	m.n.x = x;
	m.n.y = y;
	m.n.id = 'Y';
	m.n.rssi = 10;
	m.n.id = m.src;
	node.x = x;
	node.y = y;
}



void receive_hecops_message(){
	int ret;
	char * msg_two;
	char data[nic->mtu()];


	ret = nic->receive(&src, &prot, data, sizeof(data));
	if(ret > 0){
		msg_hecops = (HECOPS::Message*)data;
		cout<< "Dado Recebido X : " << msg_hecops->n.x << "\n";
		cout<< "Dado Recebido Y : " << msg_hecops->n.y << "\n";
		//while(ret<0){
		//if(node->is_anchor(msg_hecops->src)){
		//	msg_hecops->n.rssi = msg_hecops->rssi;
		//	HECOPS::Message mg;
		//	mg.src = id;
		//	mg.dst = HECOPS::EVERYBODY;
		//	mg.n = msg_hecops->n;
		//	msg_two = (char *)&(mg);
		//	ret = nic->send(NIC::BROADCAST,(NIC::Protocol) 1,msg_two,sizeof(msg_two));
		//	cout << ret << "\n";
		//}
	}
	//Alarm::delay(1000000);
}


void start_hecops(){
	if(shouldI){
		int ret;
		char * msg;
		//m->n.LQItoRSSI(r.lqi());
		msg = (char *)&(m);
		//	m->n = node;
		//	cout << "RSSI " << node.id << node.x << node.y << "\n";
		//	cout << "RSSI " << m.src << m.n.x << m.n.y << "\n";

		ret =	nic->send(NIC::BROADCAST, (NIC::Protocol) 1, msg, sizeof(HECOPS::Message));
		//	cout << ret << "\n";
		sendDataToSCADA();
		shouldI = false;
	}
}

int triHECOPS(){
	while(true){
		if(!interrupt_hecops){
			start_hecops();
		}else{
			hecops->yield();
		}
	}
	return 0;
}

int syncMotes(){
	_protocol->setInterrupt(false);
	while(true){
		if(interrupt_hecops){
			cout << "Synchronizing\n";
			synchronizeMotes();
		}
		else{
			sync->yield();
		}
	}
	return 0;
}
void shouldISend(){
	shouldI = true;
}

void interrupt(){
	if(interrupt_hecops){
		interrupt_hecops = false;
		_protocol->setInterrupt(true);
	}
	else{
		interrupt_hecops = true;
		_protocol->setInterrupt(false);
	}
}



int main(){
	rtc.setTimeStamp(1359140533);
	nic = new NIC();
	//INICIALIZADO PTP
	_ptp = startPTP();
	_protocol = new IEEE_1588(_ptp);
	_protocol->setNIC(nic);
	_protocol->initializeServo();
	//INICIALIZANDO HECOPS
	anchor('Y', 20, 20);

	Function_Handler handler_b(&shouldISend);
	Alarm alarm_b(10000000, &handler_b, -1);

	Function_Handler handler_ptp(&interrupt);
	Alarm alarm_ptp(33000000, &handler_ptp, -1);

	hecops = new Thread(&triHECOPS);
	sync = new Thread(&syncMotes);
	sync->join();

	while(true);
	return 0;

	//	//Alarm para trocar contexto para SCADABR
	//	Function_Handler handler_scada(&initialize_scada);
	//	Alarm alarm_scada(53000000, &handler_scada, -1);

}
