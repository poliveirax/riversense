// EPOS HECOPS Test Program

#include <hecops.h>
#include <utility/ostream.h>
#include <alarm.h>
#include "ieee1588.h"
#include "ieee1588_ptp.h"
#include "thread.h"
#include "radio.h"
#include "periodic_thread.h"
#include <rtc.h>

__USING_SYS

RTC rtc;
IEEE1588_PTP * _ptp;
IEEE_1588 * _protocol;
NIC * nic;
OStream cout;
HECOPS * h;
NIC::Protocol prot;
NIC::Address src;
Thread * hecops;
Thread * sync;
HECOPS::Message *msg, *m;
HECOPS::NodeList nl;
//HECOPS::Node node;
HECOPS::Node res;
Radio_Wrapper r;
HECOPS::Node me('k',0,0);
//long _x[3], _y[3], _rssi[3];
HECOPS::Node _nodes[3];
bool initialized = false;
bool interrupt_hecops;
bool shouldI;
//RESPONSAVEL PELO RECEBIMENTO DOS DADOS DO RADAR.
//TALVEZ, PODEMOS INSERIR A INFORMAÇÂO VINDA DO RADAR NA MENSAGEM E USAR NA TRIANGULACAO.

class MsgSCADA {

public:
	int msgId;
	int nodeId;
	long x_value;
	long y_value;
	long rssi_value;
	int number_of_sensors;
	unsigned long timeStamp;
} __attribute__((packed));


MsgSCADA dat;




IEEE1588_PTP * startPTP()
{
	int i;
	// Iniciando e Configurando o PTP Clock
	_ptp = new IEEE1588_PTP();
	_ptp->clock_stratum = 255;
	kout << "Iniciado com sucesso\n";
	return _ptp;
}

int synchronizeMotes(){
	if(!initialized){
		initialized = true;
		_protocol->startProtocol();
	}else{
		_protocol->synchronizing();
	}
	return 0;
}

void ReceiveFromGateway(){
	//	ret = nic->receive(&src, &prot, receive_one, sizeof(receive_one));
}

void mobile(){
	cout << "Mobile\n";
	char * send_message;
	int n;
	long x=-1, y=-1;
	int confidence;
	const long rise = 100;
	long factor=rise;
	long soma=0;
	int i=0;
	int ret;
	char receive_one[nic->mtu()];
	while(!interrupt_hecops){
		ret = nic->receive(&src, &prot, receive_one, sizeof(HECOPS::Message));
		if(ret > 0){
			msg = (HECOPS::Message*)receive_one;
			if(msg->src == msg->n.id ){
				cout << "SRC e Id são iguais \n";
				cout << nl.size << "\n";
				me.LQItoRSSI(r.lqi());
				msg->n.rssi = me.rssi;
				cout << "RSSI da MSG : " << msg->n.rssi << "\n";
				kout << msg->n.x << msg->n.y << endl;

				if(nl.get(msg->n.id) == NULL && msg->src != NULL){
					nl.insert(msg->n);
				}else{
					HECOPS::Node * n_temp = nl.get(msg->n.id);
					n_temp->rssi = msg->n.rssi;
				}

				int n = h->min(3,nl.get_size());
				cout << "N : " << n << "\n";
				HECOPS::Node * node = nl.first;

				if(n==1){
					x = node->x + msg->n.rssi * factor / rise;
					y = node->y + msg->n.rssi * factor / rise;
					confidence = nl.first->confidence * 3 / 4;
					cout << "X : "<< x << "Y : "<< y << "\n";
				}else{
					long _x[n], _y[n], _rssi[n];
					confidence = 0;
					for(int i=0; i<n; i++){
						_x[i] = node->x;
						_y[i] = node->y;
						long c; int ctri;

						kout <<"node dev " << node->dev << endl;


						if(node->dev == 0){
							c = factor;
							ctri = 0;
						}else{
							c = node->dev;
							if(node->id_tri) ctri = nl.get(node->id_tri)->confidence;
							if(ctri < 80) ctri = 0;
						}
						_rssi[i] = node->rssi*c/rise;
						kout << "rssi " << _rssi[i] << endl;
						confidence += (node->confidence * 3 + ctri) / 4;
						node = node->next;
					}
					HECOPS::Node res;

					kout << "Tamanho da Lista" << i << endl;

					h->calculate(_x,_y,_rssi,n,&res);
					x = res.x; y = res.y;
					cout << "X : "<< x << "Y : "<< y << "\n";

				}

				confidence = confidence * 4 / 15;
				me.x = x; me.y = y; me.confidence = confidence;
				msg->src = 'k'; msg->dst = HECOPS::EVERYBODY;
				msg->n = me;
				nic->send(NIC::BROADCAST,(NIC::Protocol) 1,&msg,sizeof(msg));

			}else{
				HECOPS::Node * no = nl.get(msg->src);
				if(no){
					long dist = h->distance(no->x, no->y, msg->n.x, msg->n.y);
					factor = dist*rise/msg->n.rssi;
					soma += factor; i++;
					factor = soma/i;
					//printf("### %l\n", factor);
					if(dist / 2 > h->distance(msg->n.x, msg->n.y, x, y)){ /// If triangulation is constated, deviation will be corrected
						//		no->id, no->x, no->y, msg.n.id, msg.n.x, msg.n.y, id, x, y);
						no = nl.get(msg->n.id);
						no->dev = factor;
						no->id_tri = msg->src;
						free(no);
					}
				}
			}
		}
		free(msg);
		free(send_message);
		ret = 0;
	}
}

void sendDataToSCADA(){
	char * ms;
	cout << "Sending to SCADABR\n";
	dat.nodeId = 2;
	dat.msgId = 99;
	dat.x_value = me.x;
	dat.y_value = me.y;
	dat.rssi_value = me.rssi;
	dat.number_of_sensors = nl.get_size() + 1;
	dat.timeStamp = rtc.getTimeStamp();

	ms = (char *)&(dat);
	for(int i=0;i<3;i++)
		nic->send(NIC::BROADCAST, (NIC::Protocol) 1, ms, sizeof(MsgSCADA));

	//DEBUG
	kout << dat.nodeId << dat.x_value << dat.y_value <<	dat.rssi_value << dat.number_of_sensors << dat.timeStamp << endl;

}

int triHECOPS(){
	while(true){
		if(!interrupt_hecops){
			mobile();
			if(shouldI){
				sendDataToSCADA();
				shouldI = false;
			}
		}else{
			cout << "YIELD-HECOPS\n";
			hecops->yield();
		}
	}
	return 0;
}


int syncMotes(){
	_protocol->setInterrupt(false);
	while(true){
		if(interrupt_hecops){
			cout << "Synchronizing\n";
			synchronizeMotes();
		}
		else{
			sync->yield();
		}
	}
	return 0;
}

void shouldISend(){
	shouldI = true;
}

void interrupt(){
	if(interrupt_hecops){
		interrupt_hecops = false;
		_protocol->setInterrupt(true);
	}
	else{
		interrupt_hecops = true;
		_protocol->setInterrupt(false);
	}
}




int main(){

	nic = new NIC();
	_ptp = startPTP();
	_protocol = new IEEE_1588(_ptp);
	_protocol->setNIC(nic);
	_protocol->initializeServo();

	Function_Handler handler_ptp(&interrupt);
	Alarm alarm_ptp(33000000, &handler_ptp, -1);

	Function_Handler handler_b(&shouldISend);
	Alarm alarm_b(7000000, &handler_b, -1);

	hecops = new Thread(&triHECOPS);
	sync = new Thread(&syncMotes);
	sync->join();

	while(true);
	return 0;
}
