#include <arch/arm7/tsc.h>
#include <rtc.h>
#include <alarm.h>

__USING_SYS

OStream cout;
RTC rtc;

int main() {

	ARM7_TSC t= ARM7_TSC();
	t.init();

	long long timestamp = 1346809688;
	long long before;

	RTC::Date d(2012,0,0, 0, 0, 10);
	rtc.date(d);
	rtc.setTimeStamp(rtc.seconds_since_epoch());
	cout<< "Seconds Since Epoch " <<	rtc.seconds_since_epoch() << endl;

	while(true){
		cout << "Seconds " << t.time_stamp() << endl;
		before = t.time_stamp() * 1000000/t.frequency();
		cout << "Time" << before <<"\n";
		Alarm::delay(1000000);

	}

	return 0;
}


