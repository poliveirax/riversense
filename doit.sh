#!/bin/sh
make APPLICATION=$1
/usr/local/arm/bin/arm-objcopy -I elf32-little -O binary img/$1.img img/$1.bin
python tools/emote/red-bsl.py -t /dev/ttyUSB0 -f img/$1.bin
