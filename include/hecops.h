// EPOS HECOPS Package

#ifndef __hecops
#define __hecops

#include <system/config.h>
#define NULL 0

__BEGIN_SYS

class HECOPS
{

public:
	HECOPS() {}

	static const char BASE      = '#';
	static const char EVERYBODY = '*';

	inline bool is_base(char who){
		return who == 'A';
	}

	class Node{ /// Useful node information for localization
	public:
		char id;
		long x, y, rssi;
		unsigned long rssi_sum, n;
		int confidence;
		int dev;
		char id_tri;
		Node * next;

		inline bool is_anchor(char who){
			return (who >= 'A' && who <= 'Z');
		}

		Node(){}

		Node(char id, long _x, long _y){
			confidence = is_anchor(id) ? 100 : 0;
			x = _x;
			y = _y;
			this->id = id;
			dev = -1;
			id_tri = 1;
			rssi_sum = 0;
			n = 0;
		}

		Node(Node &n){
			*this = n;
		}

		void new_rssi_reading(int reading){
			rssi_sum += reading;
			n++;
			rssi = rssi_sum / n;
		}


		void LQItoRSSI(int lqi){
			rssi = (lqi/3)-100;
		}
		bool equals(Node *n){
			return (id == n->id && x == n->x && y == n->y &&
					confidence == n->confidence && rssi == n->rssi);
		}


	}__attribute__((packed));

	struct NodeList{
		Node *first;
		int size;

		NodeList(){
			first = NULL;
			size = 0;
		}

		bool insert(Node n){
			Node *x;
			for(x = first; x!=NULL; x=x->next){
				if(n.id == x->id){
					if(n.equals(x)) {
						return false;
					} else {
						if(x->rssi != n.rssi)
							x->new_rssi_reading(n.rssi);
						n.x = x->x; n.y = x->y; n.confidence = x->confidence;
						return true;
					}
				}
			}

			Node *nw = new Node(n);
			if(first == NULL){
				first = nw; nw->next = NULL;
			}else{
				bool done = false; Node *ant = NULL;
				for(x = first; x!=NULL; x=x->next){
					if(nw->confidence > x->confidence){
						nw->next = x;
						if(x == first) first = nw;
						else ant->next = nw;
						done = true;
					}
					ant = x;
				}
				if(!done){ ant->next = nw; nw->next = NULL; }
			}
			size++; //printf("'%c' inserted\n", n.id);
			kout << "Inserido : " << n.id << endl;
			return true;
		}

		void remove(Node *n){
			if(size == 0) return;
			if(first == n){
				first = n->next;
			} else {
				Node *x, *ant = first;
				for(x=first->next; x!=NULL; x=x->next){
					if(x == n){
						ant->next = x->next;
						break;
					}
					ant = x;
				}
			}
			kout << "Removido " << n->id << endl;
			size--;
			delete n;
		}

		Node *get(char id){
			Node *x;
			for(x = first; x!=NULL; x=x->next){
				if(id == x->id) return x;
			}
			return NULL;
		}

		int get_size(){
			return size;
		}
	};


	class Message{
	public:
		int rssi;
		char src;
		char dst;
		HECOPS::Node n;

		/*Node& Node::operator=(const Node &n) {
			if (this != &n) {
				this->confidence = n.confidence;
				this->x = n.x;
				this->y = n.y;
				this->id = id;
				this->dev = n.dev;
				this->id_tri = n.id_tri;
				this->rssi_sum = n.rssi_sum;
				this->n = n.n;
			}
			return *this;
		}
		 */

	}__attribute__((packed));

	template <class T>
	T isqrt(T x){
		if(x==1 || x==0) return x;
		int i;
		T xi = x/2, xa = 0xffff;
		for(i=0; i<25; i++){
			xi = (xi+x/xi) / 2;
			if(xi == xa) break;
			xa = xi;
		}
		return xi;
	}


	template <class T>
	inline int x2(T x){
		return (x * x);
	}

	template <class T>
	T distance(T x1, T y1, T x_2, T y2){
		return isqrt(x2(x1-x_2)+x2(y1-y2));
	}

	template <class T>
	inline int min(T a, T b){
		return ( a<b ? a : b );
	}

	template <class T>
	inline int max(T a, T b){
		return ( a>b ? a : b );
	}

	template <class T>
	inline int abs(T a){
		return ( a < 0 ? -a : a );
	}

	int strlen(char *s){
		int tam = 0;
		while(*s++) tam++;
		return tam;
	}


	template <class T>
	int calculate(T x[], T y[], T d[], int n, Node *result){
		T xmax, xmin, ymin, ymax, xi, xf, yi, yf;

		xf = xmax = x[0]+d[0];
		xi = xmin = x[0]-d[0];
		yf = ymax = y[0]+d[0];
		yi = ymin = y[0]-d[0];

		for(int i=1; i<n; i++){
			T   xp = x[i]+d[i], yp = y[i]+d[i],
					xn = x[i]-d[i], yn = y[i]-d[i];

			// AREA MAXIMA
			xmin = min(xmin, xn);   xmax = max(xmax, xp);
			ymin = min(ymin, yn);   ymax = max(ymax, yp);

			// INTERSECAO
			xi = max(xi, xn);       xf = min(xf, xp);
			yi = max(yi, yn);       yf = min(yf, yp);

			if(xi >= xf) {
				xi = xmin; xf = xmax;
			}
			if(yi >= yf) {
				yi = ymin; yf = ymax;
			}
		}

		int N = 2000, w=0;
		T xe, ye, nx, ny;
		T emin = 0xffff, e, lx = xf-xi, ly = yf-yi, dx, dy;
		if(lx == 0 || ly == 0){
			return 0;
		}
		nx = isqrt(N*lx/ly);
		ny = isqrt(N*ly/lx);

		if(nx == 0 || ny == 0){
			return 0;
		}
		dx = lx / nx;  dy = ly / ny;
		if(dx==0){ dx = 1; nx = xf-xi; }
		if(dy==0){ dy = 1; ny = yf-yi; }

		T resx, resy;
		for(int i=0; i<nx;){
			for(int j=0; j<ny;){
				xe = ((T)i*dx) + xi;
				ye = ((T)j*dy) + yi;
				e=0;
				for(int k=0; k<n; k++)
					e += abs(distance(x[k],y[k],xe,ye)-d[k]);
				if(e < emin){
					emin = e;
					resx = xe; resy = ye;
				}
				w++;
				while((++j*dy)+yi == ye);
			}
			while((++i*dx)+xi == xe);
		}

		result->x = resx;
		result->y = resy;

		return 1;
	}




};

__END_SYS

#ifdef __HECOPS_H
#include __HECOPS_H
#endif

#endif

