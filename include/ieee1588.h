// EPOS IEEE 1588 Protocol Declarations

// This work is licensed under the EPOS Software License v1.0.
// A copy of this license is available at the EPOS system source tree root.
// A copy of this license is also available online at:
// http://epos.lisha.ufsc.br/EPOS+Software+License+v1.0
// Note that EPOS Software License applies to both source code and executables.

#ifndef __ieee1588_h
#define __ieee1588_h

#include "ieee1588_bmc.h"
#include "ieee1588_servo.h"
#include "ieee1588_msg.h"
#include "ieee1588_ptp.h"
#include <utility/random.h>
#include <thread.h>
#include <periodic_thread.h>



__BEGIN_SYS

class IEEE_1588
{

public:

#define PTP_SYNC_INTERVAL_TIMEOUT(x) (1<<((x)<0?0:(x)))
#define PTP_SYNC_RECEIPT_TIMEOUT(x)  (10*(1<<((x)<0?0:(x))))


	enum {
		PTP_INITIALIZING=0,
		PTP_FAULTY = 1,
		PTP_DISABLED= 2,
		PTP_LISTENING= 3,
		PTP_PRE_MASTER= 4,
		PTP_MASTER= 5,
		PTP_PASSIVE= 6,
		PTP_UNCALIBRATED = 7,
		PTP_SLAVE = 8
	};

	IEEE_1588(IEEE1588_PTP * _ptp);
	~IEEE_1588();
	void startProtocol();
	void initializeServo();
	bool doInit();
	void doState();
	void toState(char state);
	void initData();
	void startTimer();
	void initClock();
	void initClockVars();
	void synchronizing();
	void setNIC(NIC * n);
	bool initialized;
	void setSendSync(bool i);
	void setInterrupt(bool i);
	bool getInterrupt();
	bool getSendSync();
	IEEE1588_PTP * _protocol_ptp;
	IEEE1588_Msg * _msg;
	IEEE1588_BMC * _bmc;
	NIC * nic;

};

__END_SYS

#endif

