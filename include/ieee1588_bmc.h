// EPOS IEEE 1588 BMC Declarations

// This work is licensed under the EPOS Software License v1.0.
// A copy of this license is available at the EPOS system source tree root.
// A copy of this license is also available online at:
// http://epos.lisha.ufsc.br/EPOS+Software+License+v1.0
// Note that EPOS Software License applies to both source code and executables.

#ifndef __ieee1588_bmc_h
#define __ieee1588_bmc_h

#include "ieee1588_ptp.h"

__BEGIN_SYS

class IEEE1588_BMC
{

public:

	#define CLOCK_FOLLOWUP     true
	#define CLOCK_FOLLOWUP_RAW false
	#define INITIALIZABLE      true
	#define BURST_ENABLED      false
	#define EXTERNAL_TIMING    false
	#define BOUNDARY_CLOCK     false
	#define NUMBER_PORTS       1
	#define PTP_DELAY_REQ_INTERVAL              4
	#define PTP_FOREIGN_MASTER_THRESHOLD        2
	#define PTP_FOREIGN_MASTER_TIME_WINDOW(x)   (4*(1<<((x)<0?0:(x))))
	#define PTP_RANDOMIZING_SLOTS               18

	IEEE1588_BMC();
    ~IEEE1588_BMC();
    void initData(IEEE1588_PTP * _ptp);
    void master(IEEE1588_PTP * _ptp);

private:

};

__END_SYS

#endif
