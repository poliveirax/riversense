// EPOS IEEE 1588 Handler Declarations

// This work is licensed under the EPOS Software License v1.0.
// A copy of this license is available at the EPOS system source tree root.
// A copy of this license is also available online at:
// http://epos.lisha.ufsc.br/EPOS+Software+License+v1.0
// Note that EPOS Software License applies to both source code and executables.

#ifndef __ieee1588_handler_h
#define __ieee1588_handler_h

#include <ieee1588.h>
#include <ieee1588_msg.h>
#include <ieee1588_ptp.h>
#include <nic.h>
#include <tsc.h>


__BEGIN_SYS

class IEEE1588_Handler
{

public:

	IEEE1588_Handler(IEEE1588_PTP * _ptp,IEEE_1588 * _p, NIC * nic, IEEE1588_Msg * msg);
    ~IEEE1588_Handler();
    void handle();
    void handle_listener();
    void handleSync(char * data);
    void handleFollowUp();
    void handleDelayReq(char * data);
    void handleDelayResp(char * data);
    void handleAnnounce();
    void issueSync();
    void issueFollowup(IEEE1588_Msg::TimeInternal * _time);
    void issueDelayReq();
    void issueDelayResp();
    void issueAnnounce();
    IEEE1588_Msg::MsgAnnounce * addForeign(char * _x, IEEE1588_Msg::Header * _msg_header);  // AKB: added for v2

private:
    IEEE1588_PTP * _ptp;
    IEEE_1588 * _protocol;
    IEEE1588_Servo * _servo;
    NIC * _nic;
    IEEE1588_Msg * _msg;
    int messageType;
    ARM7_TSC * _time;
};

__END_SYS

#endif



