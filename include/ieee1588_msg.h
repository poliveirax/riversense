// EPOS IEEE 1588 Message Declarations

// This work is licensed under the EPOS Software License v1.0.
// A copy of this license is available at the EPOS system source tree root.
// A copy of this license is also available online at:
// http://epos.lisha.ufsc.br/EPOS+Software+License+v1.0
// Note that EPOS Software License applies to both source code and executables.

#ifndef __ieee1588_msg_h
#define __ieee1588_msg_h

#include "ieee1588_ptp.h"
#include "tsc.h"

__BEGIN_SYS

class IEEE1588_Msg
{

public:

	class PortIdentity
	{
	public:
		char clockIdentity[8];
		u16 portNumber;
	}__attribute__((packed));

	class Header
	{
	public:
		u8    transportSpecificAndMessageType;      // 00       1
		u8    reserved1AndVersionPTP;               // 01       1
		u16   messageLength;                        // 02       2
		u8    domainNumber;                         // 04       1
		u16   sequenceId;                           // 05       2
		u8    control;                              // 07       1
		u32 sourcePortIdentity;						// 11       4
	} __attribute__((packed));

	class TimeRepresentation
	{
	public:
		u16 epoch_number;
		u32 seconds;
		u32  nanoseconds;
	} __attribute__((packed));

	class MsgSync
	{
	public:
		IEEE1588_Msg::Header head;					// 0       12
		TimeRepresentation originTimestamp;			// 11      10
	} __attribute__((packed));

	class MsgDelayReq
	{
	public:// Offset  Length (bytes)
		IEEE1588_Msg::Header head;
		TimeRepresentation originTimestamp;
		// 34       10
	} __attribute__((packed));


	class MsgAnnounce
	{
	public:
		IEEE1588_Msg::Header head;
		// Offset  Length (bytes)
		TimeRepresentation originTimestamp;
		short            currentUTCOffset;
		u8            reserved;
		u8            grandmasterPriority1;
		//ClockQuality         grandmasterClockQuality;
		u8            grandmasterPriority2;
		char                grandmasterIdentity[8];
		u16           stepsRemoved;
		//Enumeration8         timeSource;

	} __attribute__((packed));

	class MsgFollowUp
	{
	public:
		TimeRepresentation preciseOriginTimestamp;
	} __attribute__((packed));

	class MsgDelayResp
	{
	public:
		IEEE1588_Msg::Header head;
		TimeRepresentation receiveTimestamp;
		u32 requestingPortIdentity;
	} __attribute__((packed));


	class TimeInternal
	{
	public:
		int seconds;
		int nanoseconds;
	};


	enum {
		SYNC_RECEIPT_TIMER=0,
		SYNC_INTERVAL_TIMER,
		ANNOUNCE_RECEIPT_TIMER,       // AKB: Added for V2
		ANNOUNCE_INTERVAL_TIMER,      // AKB: Added for V2
		PDELAY_INTERVAL_TIMER,        // AKB: Added for V2
		QUALIFICATION_TIMER,
		TIMER_ARRAY_SIZE               /* these two are non-spec */
	};

#define SYNC_MESSAGE                 0x0
#define DELAY_REQ_MESSAGE            0x1
#define FOLLOWUP_MESSAGE             0x8
#define DELAY_RESP_MESSAGE           0x9
#define ANNOUNCE_MESSAGE             0xB
#define SYNC_LENGTH                 44
#define DELAY_REQ_LENGTH            44
#define FOLLOWUP_LENGTH             44
#define DELAY_RESP_LENGTH           54
#define ANNOUNCE_LENGTH             64
#define SYNC_CONTROL                 0x00
#define DELAY_REQ_CONTROL            0x01
#define FOLLOWUP_CONTROL             0x02
#define DELAY_RESP_CONTROL           0x03
#define ALL_OTHERS_CONTROL           0x05
#define HEADER_LENGTH             40
#define SYNC_PACKET_LENGTH        124
#define DELAY_REQ_PACKET_LENGTH   124
#define FOLLOW_UP_PACKET_LENGTH   52
#define DELAY_RESP_PACKET_LENGTH  60
#define ALTERNATE_MASTER_FLAG        0x01  // Bit 0
#define TWO_STEP_FLAG                0x02  // Bit 1
#define UNICAST_FLAG                 0x04  // Bit 2
#define PTP_PROFILE_SPECIFIC_1_FLAG  0x20  // Bit 5
#define PTP_PROFILE_SPECIFIC_2_FLAG  0x40  // Bit 6
#define RESERVED_FLAG                0x80  // Bit 7
#define ANNOUNCE_LI_61                    0x01
#define ANNOUNCE_LI_59                    0x02
#define ANNOUNCE_CURRENT_UTC_OFFSET_VALID 0x04
#define ANNOUNCE_PTP_TIMESCALE            0x08
#define ANNOUNCE_TIME_TRACEABLE           0x10
#define ANNOUNCE_FREQUENCY_TRACEABLE      0x20
#define LOGMEAN_UNICAST               0x7f  // For unicast SYNC, FOLLOWUP and DELAY_RESP
#define LOGMEAN_DELAY_REQ             0x7f
#define TS_ATOMIC_CLOCK         0x10
#define TS_GPS                  0x20
#define TS_TERRESTRIAL_RADIO    0x30
#define TS_PTP                  0x40
#define TS_NTP                  0x50
#define TS_HAND_SET             0x60
#define TS_OTHER                0x90
#define TS_INTERNAL_OSCILLATOR  0xA0

public:

	IEEE1588_Msg();
	~IEEE1588_Msg();

	void msgPackHeader(IEEE1588_Msg::Header * header, IEEE1588_PTP * _ptp);
	u8 msgGetPtpVersion (char * buf);
	void msgUnpackHeader(char * buf);
	void msgUnpackSync(char * buf);
	void msgUnpackAnnounce(char * buf);
	void msgUnpackFollowUp(char * buf, MsgFollowUp *follow);
	void msgUnpackDelayResp(char * buf, MsgDelayResp   *resp );
	void msgUnpackDelayReq(char *buf, IEEE1588_Msg::MsgDelayResp   *resp );
	void msgPackSync(IEEE1588_PTP * _ptp, ARM7_TSC * time);
	void msgPackAnnounce(IEEE1588_PTP * _ptp);
	void msgPackDelayReq(IEEE1588_PTP * _ptp, ARM7_TSC * time);
	void msgPackDelayReq_Aux(IEEE1588_PTP * _ptp, ARM7_TSC * time);
	void msgPackFollowUp(IEEE1588_PTP * _ptp );
	void msgPackDelayResp(IEEE1588_PTP * _ptp, ARM7_TSC * time);
	Header * get_temp_header();
	Header * get_header();

public:
	Header _header;
	MsgSync _sync;
	TimeRepresentation * _time;
	TimeInternal * _time_internal;
	MsgAnnounce  _announce;
	MsgFollowUp  _follow;
	MsgDelayResp  _delay_resp;
	MsgDelayReq  _delay_req;

};

__END_SYS

#endif
