// EPOS IEEE1588 PTP Abstraction Declarations

// This work is licensed under the EPOS Software License v1.0.
// A copy of this license is available at the EPOS system source tree root.
// A copy of this license is also available online at:
// http://epos.lisha.ufsc.br/EPOS+Software+License+v1.0
// Note that EPOS Software License applies to both source code and executables.

#ifndef __ieee1588_ptp_h
#define __ieee1588_ptp_h

#include <utility/random.h>
#include <alarm.h>
#include <utility/handler.h>

__BEGIN_SYS

typedef unsigned char   u8;
typedef unsigned short u16;
typedef unsigned long  u32;
typedef signed   long  s32;

class IEEE1588_PTP
{
public:

	IEEE1588_PTP() {
		clock_stratum = 0;
	}

	typedef struct
	{
		u32 seconds;
		u32 nanoseconds;
	} TimeInternal;

	typedef struct
	{
		u8        clockClass;
		char     clockAccuracy;
		u16       offsetScaledLogVariance;
	} ClockQuality;

	enum {
		SYNC_RECEIPT_TIMER=0,
		SYNC_INTERVAL_TIMER,
		ANNOUNCE_RECEIPT_TIMER,
		ANNOUNCE_INTERVAL_TIMER,
		PDELAY_INTERVAL_TIMER,
		QUALIFICATION_TIMER,
	};

	typedef struct
	{
		int  nsec_prev;
		int  y;
	} offset_from_master_filter;

	typedef struct
	{
		int  nsec_prev;
		int  y;
		int  s_exp;
	} one_way_delay_filter;

	offset_from_master_filter  ofm_filter;
	one_way_delay_filter       owd_filter;
	u16      currentUtcOffset;
	u16      epochNumber;
	short     ap, ai;
	short     s;

	TimeInternal  inboundLatency, outboundLatency;
	bool       slaveOnly;
	bool       slave_only;
	bool       halfEpoch;
	int     baseAdjustValue;
	bool       rememberAdjustValue;
	char      announceInterval;
	unsigned int    clock_stratum;
	bool      clock_followup_capable;
	bool      external_timing;
	long     sync_interval;
	ClockQuality  clock_quality;
	u8     priority1;
	u8     priority2;
	u8     domain_number;
	u32 correctionField;
	TimeInternal  offset_from_master;
	TimeInternal  one_way_delay;
	TimeInternal  mean_path_delay;        /* V2 */
	u16    parent_last_sync_sequence_number;
	bool       parent_followup_capable;
	bool       parent_external_timing;
	bool       parent_stats;                  /* V1 & V2 */
	int     observed_drift;                /* V1 & V2 observedParentClockPhaseChangeRate */
	bool       utc_reasonable;
	u8     grandmaster_communication_technology;
	char         grandmaster_uuid_field[6];
	u16    grandmaster_port_id_field;
	u8     grandmaster_stratum;
	char         grandmaster_identifier[4];
	bool       grandmaster_preferred;
	bool       grandmaster_is_boundary_clock;
	u16    grandmaster_sequence_number;
	char         parent_clock_identity[8];      /* V2 uses EUI-64 */
	u16    parent_v2_variance;            /* V2 uses UInteger16 */
	char         grandmaster_clock_identity[8]; /* V2 uses EUI-64 */
	u16    observed_v2_variance;          /* V2 uses UInteger16 */
	u8     grandmaster_priority1;
	u8     grandmaster_priority2;
	ClockQuality  grandmaster_clock_quality;     /* V2, includes Class, Accuracy and Variance */
	u16    parent_last_announce_sequence_number;
	short     current_utc_offset;       /* V1 & V2 */
	bool       leap_59;                  /* V1 & V2 */
	bool       leap_61;                  /* V1 & V2 */
	u16    epoch_number;
	bool       current_utc_offset_valid;
	bool       time_traceable;
	bool       frequency_traceable;
	bool       ptp_timescale;
	char  time_source;
	u8     _state;                                       /* V1 & V2 */
	u16    last_sync_tx_sequence_number;
	u16    last_general_event_sequence_number;
	u8     port_communication_technology;
	u16    port_id_field;
	bool       burst_enabled;
	char         port_clock_identity[8];  /* V2 uses EUI-64 */
	char      delay_req_interval;
	char      announce_receipt_timeout;
	char  delay_mechanism;
	u8     version_number;
	char      sync_receipt_timeout; /* IEEE 802.1AS has both a sync and announce timeout */
	u16    last_announce_tx_sequence_number;
	u16    last_delay_req_tx_sequence_number;  // Used for both delay and pdelay
	u8     current_msg_version;
	u8     msg_type;
	u8     rx_transport_specific;
	short  foreign_record_i;
	short  foreign_record_best;
	bool    record_update;
	u32 random_seed;
	char * msgObuf;
	char * msgIbuf;
	char outputBuffer[26];  /**< Output buffer array: Packet size plus size for MAC header */
	char inputBuffer[26];   /**< Output buffer array: Packet size plus size for MAC header */
	TimeInternal  master_to_slave_delay;
	TimeInternal  slave_to_master_delay;
	TimeInternal  t1_sync_tx_time;      /**< Time from master from SYNC (1 step) or FOLLOW UP (2 step) */
	TimeInternal  t2_sync_rx_time;      /**< Time at slave of inbound SYNC message */
	TimeInternal  t3_delay_req_tx_time; /**< Time at slave of outbound DELAY REQ */
	TimeInternal  t4_delay_req_rx_time; /**< Time from master from DELAY RESP */
	TimeInternal  sync_correction;
	TimeInternal  t1_sync_delta_time;  /**< time between transmitted sync messages */
	TimeInternal  t2_sync_delta_time;  /**< time between received    sync messages */
	bool       pdelay_resp_rx_two_step_flag;  // Used to select proper equation to use
	short  Q;
	short  R;
	bool     sentDelayReq;     /**< NOTE: Used for both Delay and PDelay request */
	u16  sentDelayReqSequenceId;
	bool     sentSync;         /**< Sync Transmitted from Application */
	bool message_activity;
	int     lastAdjustValue;      /**< AKB: for storing calculated adjust value */
	char     tx_transport_specific; /* For setting of field in first byte of all V2 messages */
	bool syncAlarmSet;
	bool announceAlarmSet;
	int initialized;
	bool synchronized;
};



__END_SYS

#endif

