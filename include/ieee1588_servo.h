// EPOS IEEE 1588 Servo Declarations

// This work is licensed under the EPOS Software License v1.0.
// A copy of this license is available at the EPOS system source tree root.
// A copy of this license is also available online at:
// http://epos.lisha.ufsc.br/EPOS+Software+License+v1.0
// Note that EPOS Software License applies to both source code and executables.

#ifndef __ieee1588_servo_h
#define __ieee1588_servo_h

#include "ieee1588_bmc.h"
#include "ieee1588_msg.h"
#include "ieee1588_ptp.h"
#include <tsc.h>

__BEGIN_SYS

class IEEE1588_Servo
{

public:
	IEEE1588_Servo();
    ~IEEE1588_Servo();
    void setDate();
    unsigned long getSeconds();
    unsigned long getMicro();
    void setTimer(ARM7_TSC * t);
    void initClockVars(IEEE1588_PTP * _ptp);
    void initTimer(int seconds, u32 microseconds);
    void calculateOffset(IEEE1588_PTP * ptp);
    void calculaDelay(IEEE1588_PTP * ptp);
    void adjustTime(IEEE1588_PTP * ptp);
    RTC rtc;
private:
    ARM7_TSC * _timer;

};

__END_SYS

#endif

