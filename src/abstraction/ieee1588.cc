// EPOS IEEE 1588 Protocol Implementation

// This work is licensed under the EPOS Software License v1.0.
// A copy of this license is available at the EPOS system source tree root.
// A copy of this license is also available online at:
// http://epos.lisha.ufsc.br/EPOS+Software+License+v1.0
// Note that EPOS Software License applies to both source code and executables.

#include <ieee1588.h>
#include <ieee1588_ptp.h>
#include <ieee1588_servo.h>
#include <ieee1588_handler.h>
#include <ieee1588_bmc.h>
#include <ieee1588_msg.h>
#include <utility/ostream.h>
#include <nic.h>
#include <alarm.h>
#include <timer.h>
#include <periodic_thread.h>


__BEGIN_SYS

// IEEE1588 Protocol class attributes
OStream a;
IEEE1588_Handler * hand;
Thread * issue_sync;
Thread * handle_sync;
IEEE1588_Servo * _servo;
bool sendSync;
bool interrupt;

// Methods

IEEE_1588::IEEE_1588(IEEE1588_PTP * _ptp)
{
	_protocol_ptp = _ptp;
}

IEEE_1588::~IEEE_1588()
{
}

void IEEE_1588::initializeServo(){
	_servo = new IEEE1588_Servo();
	toState(PTP_INITIALIZING);
}

void IEEE_1588::startProtocol(){
	kout << _protocol_ptp->_state << "\n";
	while(!interrupt){
		if(_protocol_ptp->_state != PTP_INITIALIZING)
			doState();
		if(_protocol_ptp->_state == PTP_INITIALIZING)
			doInit();
	}
}

void IEEE_1588::toState(char state){
	_protocol_ptp->message_activity = true;
	switch (_protocol_ptp->_state)
	{
	case PTP_MASTER:
		break;

	case PTP_SLAVE:
		break;

	default:
		break;
	}
	switch (state)
	{
	case PTP_INITIALIZING:
		kout <<"PTP INITIALIZING \n";
		_protocol_ptp->_state = PTP_INITIALIZING;
		break;

	case PTP_FAULTY:
		kout <<"PTP FAULTY \n";
		_protocol_ptp->_state = PTP_FAULTY;
		break;

	case PTP_DISABLED:
		kout <<"PTP DISABLED \n";
		_protocol_ptp->_state = PTP_DISABLED;
		break;

	case PTP_LISTENING:
		kout <<"PTP LISTENING \n";
		_protocol_ptp->_state = PTP_LISTENING;
		break;

	case PTP_MASTER:
		kout <<"PTP MASTER \n";
		_protocol_ptp->_state = PTP_MASTER;
		break;

	case PTP_PASSIVE:
		kout <<"PTP PASSIVE \n";
		_protocol_ptp->_state = PTP_PASSIVE;
		break;

	case PTP_UNCALIBRATED:
		kout <<"PTP UNCALIBRATED \n";
		_protocol_ptp->_state = PTP_UNCALIBRATED;
		break;

	case PTP_SLAVE:
		kout <<"PTP SLAVE \n";
		_protocol_ptp->Q = 0;
		_protocol_ptp->R = 8;
		_protocol_ptp->t3_delay_req_tx_time.seconds = 0;
		_protocol_ptp->t3_delay_req_tx_time.nanoseconds = 0;
		_protocol_ptp->t4_delay_req_rx_time.seconds = 0;
		_protocol_ptp->t4_delay_req_rx_time.nanoseconds = 0;
		_protocol_ptp->_state = PTP_SLAVE;
		break;

	default:
		kout <<"ESTADO INVALIDO \n";
		break;
	}

}

void IEEE_1588::setSendSync(bool i){
	sendSync=i;
}

void IEEE_1588::setInterrupt(bool i){
	interrupt = i;
}

int issueSyncMessages(){

	while(true){
		kout << "Envia Msg de Sync\n";
		hand->issueSync();
		sendSync = false;
		issue_sync->yield();
	}
	return 0;
}

void syncAlarm(){
	sendSync = true;
}

int handleMessages(){
	while(true){
		//		kout << sendSync << interrupt << "\n";
		if(sendSync){
			handle_sync->yield();
		}
		if(interrupt){
			break;
		}
		hand->handle();
	}
	return 0;
}

int handleListenerMessages(){
	while(true){
		//		kout << sendSync << interrupt << "\n";
		if(sendSync){
			handle_sync->yield();
		}
		if(interrupt){
			break;
		}
		hand->handle_listener();
	}
	return 0;

}

void IEEE_1588::synchronizing(){
	kout << "Handling Messages\n";
	if(_protocol_ptp->_state == PTP_SLAVE || _protocol_ptp->_state == PTP_MASTER)
		handleMessages();
	if(_protocol_ptp->_state == PTP_LISTENING)
		handleListenerMessages();
}

void IEEE_1588::setNIC(NIC * n){
	this->nic = n;
}

bool IEEE_1588::doInit(){
	_protocol_ptp->msgObuf = &(_protocol_ptp->outputBuffer[200]);
	_protocol_ptp->msgIbuf = &(_protocol_ptp->inputBuffer[200]);
	//	_bmc = new IEEE1588_BMC();
	//	_bmc->initData(_protocol_ptp);
	//	_bmc->master(_protocol_ptp);
	_msg = new IEEE1588_Msg();

	if(_protocol_ptp->clock_stratum == 4){
		sendSync = false;
		issue_sync = new Thread(&issueSyncMessages);
		handle_sync = new Thread(&handleMessages);
		toState(PTP_MASTER);
	}
	else if(_protocol_ptp->clock_stratum == 100)
		toState(PTP_LISTENING);
	else if(_protocol_ptp->slaveOnly || _protocol_ptp->clock_stratum == 255){
		toState(PTP_SLAVE);
	}
	hand = new IEEE1588_Handler(_protocol_ptp, this, this->nic, _msg);
	return true;

}

void IEEE_1588::doState(){
	kout << "Do State\n";
	kout << _protocol_ptp->_state << "\n";
	char state;
	_protocol_ptp->message_activity = false;
	switch (_protocol_ptp->_state)
	{
	case PTP_FAULTY:
		toState(PTP_INITIALIZING);
		return;
	case PTP_LISTENING:{
		handleListenerMessages();
		break;
	}
	case PTP_UNCALIBRATED:
	case PTP_PASSIVE:
	case PTP_SLAVE:{
		handleMessages();
		break;
	}
	case PTP_MASTER:{
		Function_Handler handler_a(&syncAlarm);
		Alarm alarm_a(_protocol_ptp->sync_interval, &handler_a, -1);
		issue_sync->join();
		break;
	}
	case PTP_DISABLED:
		handleMessages();
		break;

	default:
		break;
	}
}

bool IEEE_1588::getInterrupt(){
	return interrupt;
}

bool IEEE_1588::getSendSync(){
	return sendSync;
}

__END_SYS

