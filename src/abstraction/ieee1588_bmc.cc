// EPOS IEEE 1588 BMC Implementation

// This work is licensed under the EPOS Software License v1.0.
// A copy of this license is available at the EPOS system source tree root.
// A copy of this license is also available online at:
// http://epos.lisha.ufsc.br/EPOS+Software+License+v1.0
// Note that EPOS Software License applies to both source code and executables.

#include <ieee1588_bmc.h>
#include <ieee1588_msg.h>
#include <ieee1588.h>
#include <ieee1588_ptp.h>
#include <utility/ostream.h>
#include <nic.h>

__BEGIN_SYS

// Class attributes
OStream bcout;

// This_IEEE1588 BMC class attributes

// Methods

IEEE1588_BMC::IEEE1588_BMC()
{
}

IEEE1588_BMC::~IEEE1588_BMC()
{
}

void IEEE1588_BMC::initData(IEEE1588_PTP * _ptp)
{
	int ret;

  /*if (_ptp->ptp8021AS)
    _ptp->tx_transport_specific = 1;
  else
    _ptp->tx_transport_specific = 0;

  if(_ptp->slaveOnly)
    _ptp->clock_stratum = 255;

  _ptp->last_sync_tx_sequence_number       = 0;
  _ptp->last_general_event_sequence_number = 0;
  _ptp->burst_enabled                      = BURST_ENABLED;
  _ptp->clock_communication_technology = _ptp->port_communication_technology;
  _ptp->clock_port_id_field    = 0;
  if (_ptp->ptp8021AS)
      _ptp->clock_followup_capable = CLOCK_FOLLOWUP_RAW;
  else
  _ptp->clock_followup_capable = CLOCK_FOLLOWUP;


  _ptp->initializable          = INITIALIZABLE;
  _ptp->external_timing        = EXTERNAL_TIMING;
  _ptp->is_boundary_clock      = BOUNDARY_CLOCK;
  _ptp->number_ports           = NUMBER_PORTS;
  _ptp->number_foreign_records = 0;
  _ptp->current_utc_offset     = _ptp->currentUtcOffset;
  _ptp->epoch_number           = _ptp->epochNumber;
  _ptp->random_seed            = _ptp->port_uuid_field[PTP_UUID_LENGTH-1];
  _ptp->last_announce_tx_sequence_number    = 0;
  _ptp->last_delay_req_tx_sequence_number   = 0;  // Used for both Delay and PDelay request
 // _ptp->last_pdelay_req_rx_sequence_number  = 0;
  //_ptp->last_pdelay_resp_tx_sequence_number = 0;
  if (_ptp->preferred)
	  _ptp->priority1 = 127;
  else
	  _ptp->priority1 = 128;
  _ptp->priority2 = 128;
  _ptp->delay_req_interval  = PTP_DELAY_REQ_INTERVAL;
//  _ptp->pdelay_req_interval = PTP_DELAY_REQ_INTERVAL;
  switch(_ptp->clock_stratum)
    {
      case 0:
    	  ret =  6;
    	  break;
      case 1:
    	  ret =  9;
    	  break;
      case 2:
    	  ret =  10;
    	  break;
      case 3:
    	  ret = 248;
    	  break;
      case 4:
    	  ret = 251;
          break;
      case 255:
        ret =  255;
        break;
    }
  if (ret == -1)
  {
    _ptp->clock_stratum            = 4;  /* Force stratum to 4 */
    /*_ptp->clock_quality.clockClass = 251;
  }
  else
    _ptp->clock_quality.clockClass = ret;

  _ptp->clock_quality.offsetScaledLogVariance = _ptp->clock_v1_variance;
  //******* VERIFICAR COM CALMA
  /*
  ret = v1_clock_identifier_to_v2( _ptp->clock_identifier,
                                           &_ptp->clock_quality.clockAccuracy,
                                           &_ptp->time_source
                                          );
*/
}

/*Local clock is becoming Master. Table 13 (9.3.5) of the spec.*/
void IEEE1588_BMC::master(IEEE1588_PTP * _ptp)
{
	  /* Default data set */
//	  _ptp->steps_removed                  = 0;  /* v1 & v2 */
//	  _ptp->offset_from_master.seconds     = 0;  /* v1 & v2 */
//	  _ptp->offset_from_master.nanoseconds = 0;  /* v1 & v2 */
//	  _ptp->one_way_delay.seconds          = 0;  /* v1 & v2 (meanPathDelay) */
//	  _ptp->one_way_delay.nanoseconds      = 0;
//	  /* Parent data set */
//	  _ptp->parent_communication_technology  = _ptp->clock_communication_technology; /* v1 */
//	  _ptp->parent_port_id                   = _ptp->clock_port_id_field; /* v1 & v2 */
//	  _ptp->parent_last_sync_sequence_number = 0;
//	  _ptp->parent_followup_capable          = _ptp->clock_followup_capable;
//	  _ptp->parent_external_timing           = _ptp->external_timing;
//	  _ptp->parent_v1_variance               = _ptp->clock_v1_variance;
//	  _ptp->grandmaster_communication_technology = _ptp->clock_communication_technology; /*v1*/
//	  _ptp->grandmaster_port_id_field        = _ptp->clock_port_id_field; /* v1 & v2 */
//	  _ptp->grandmaster_stratum              = _ptp->clock_stratum;
//	  _ptp->grandmaster_v1_variance          = _ptp->clock_v1_variance;
//	  _ptp->grandmaster_preferred            = _ptp->preferred;
//	  _ptp->grandmaster_is_boundary_clock    = _ptp->is_boundary_clock;
//	  _ptp->grandmaster_sequence_number      = _ptp->last_sync_tx_sequence_number;
//	  /* AKB: More V2 stuff */
//	  _ptp->grandmaster_priority1            = _ptp->priority1;
//	  _ptp->grandmaster_priority2            = _ptp->priority2;

}


__END_SYS
