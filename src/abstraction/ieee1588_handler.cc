// EPOS IEEE 1588 Handler Implementation

// This work is licensed under the EPOS Software License v1.0.
// A copy of this license is available at the EPOS system source tree root.
// A copy of this license is also available online at:
// http://epos.lisha.ufsc.br/EPOS+Software+License+v1.0
// Note that EPOS Software License applies to both source code and executables.

#include <ieee1588_handler.h>
#include <alarm.h>
#include <ieee1588_ptp.h>
#include <machine.h>
#include <utility/string.h>
#include <utility/malloc.h>
#include <thread.h>
#include <utility/random.h>
#include <rtc.h>

__BEGIN_SYS
RTC _rtc;
OStream handler_out;
// Class attributes
IEEE1588_Msg * msg;
NIC::Protocol prot;
NIC::Address src;
IEEE1588_Msg::Header * h;
IEEE1588_Msg::MsgSync * sync;
IEEE1588_Msg::MsgDelayResp * resp;
IEEE1588_Msg::MsgDelayReq * req;

short temp_debug_issue_delay_counter=0;

// This IEEE1588 Handler class attributes

// Methods

IEEE1588_Handler::IEEE1588_Handler(IEEE1588_PTP * ptp, IEEE_1588 * _prot, NIC * nic, IEEE1588_Msg * msg){
	_protocol = _prot;
	_ptp = ptp;
	_nic = nic;
	_servo = new IEEE1588_Servo();
	_time = new ARM7_TSC();
	_time->init();
	_msg = msg;
}

IEEE1588_Handler::~IEEE1588_Handler(){

}

void IEEE1588_Handler::handle(){
	unsigned long timestamp;
	char data[_nic->mtu()];
	int ret;
	ret = 0;
	while(((_nic->receive(&src, &prot, data, sizeof(data)) < 0) && !_protocol->getInterrupt() && !_protocol->getSendSync()));
	//	if(ret > 0){
	timestamp = _time->time_stamp() * 1000000 / _time->frequency();
	h= (IEEE1588_Msg::Header*)data;
	messageType = h->transportSpecificAndMessageType;
	//		kout << "messageType" << messageType <<"\n";
	switch(messageType)
	{
	case 5:{
		_protocol->_protocol_ptp->t2_sync_rx_time.seconds = _rtc.getTimeStamp();
		_protocol->_protocol_ptp->t2_sync_rx_time.nanoseconds = timestamp;
		handleSync(data);
		break;
	}
	case 8:{
		kout << "follow up Received\n";
		break;
	}
	case 1:{
		_protocol->_protocol_ptp->t4_delay_req_rx_time.seconds = _rtc.getTimeStamp();
		_protocol->_protocol_ptp->t4_delay_req_rx_time.nanoseconds = timestamp;
		handleDelayReq(data);
		break;
	}
	case 9:{
		handleDelayResp(data);
		break;
	}
	}
	//	}

	free(data);
}

void IEEE1588_Handler::handle_listener(){
	unsigned long timestamp;
	char data[_nic->mtu()];
	int ret;
	ret = 0;
	ret = _nic->receive(&src, &prot, data, sizeof(data));
	if(ret > 0){
		h= (IEEE1588_Msg::Header*)data;
		messageType = h->transportSpecificAndMessageType;
		handler_out << messageType << "\n";
		switch(messageType)
		{
		case 5:{
			handleSync(data);
			break;
		}
		case 8:{
			kout << "follow up Received\n";
			break;
		}
		case 11:{
			handleDelayReq(data);
			break;
		}
		case 1:{
			handleDelayReq(data);
			break;
		}
		case 9:{
			handleDelayResp(data);
			break;
		}
		}
	}
	free(data);
}

//HANDLERS

void IEEE1588_Handler::handleSync(char * data){
	sync = (IEEE1588_Msg::MsgSync*)data;
	bool sync_source_ok;
	u16 sequence_delta;
	bool current_sequence;
	switch(_protocol->_protocol_ptp->_state)
	{
	case IEEE_1588::PTP_FAULTY:
	case IEEE_1588::PTP_INITIALIZING:
	case IEEE_1588::PTP_LISTENING:
		sequence_delta = sync->head.sequenceId - _protocol->_protocol_ptp->parent_last_sync_sequence_number;
		if(sequence_delta == 0)
		{
			kout << "handleSync: Possible duplicate sequence %u received!, ignoring"<< sync->head.sequenceId << "\n";
			return;
		}
		if  (sequence_delta != 0){
			_protocol->_protocol_ptp->parent_last_sync_sequence_number = sync->head.sequenceId;
			_protocol->_protocol_ptp->t1_sync_tx_time.seconds = sync->originTimestamp.seconds;
			_protocol->_protocol_ptp->t1_sync_tx_time.nanoseconds = sync->originTimestamp.nanoseconds;
			_protocol->_protocol_ptp->Q = 0;
		}
		break;
	case IEEE_1588::PTP_DISABLED:
		kout << "handleSync: FAULTY, INITIALIZING or DISABLED \n";
		return;
	case IEEE_1588::PTP_UNCALIBRATED:
	case IEEE_1588::PTP_SLAVE:
		sequence_delta = sync->head.sequenceId - _protocol->_protocol_ptp->parent_last_sync_sequence_number;
		if(sequence_delta == 0)
		{
			//kout << "handleSync: Possible duplicate sequence %u received!, ignoring"<< sync->head.sequenceId << "\n";
			return;
		}
		if  (sequence_delta != 0){
			_protocol->_protocol_ptp->parent_last_sync_sequence_number = sync->head.sequenceId;
			_protocol->_protocol_ptp->t1_sync_tx_time.seconds = sync->originTimestamp.seconds;
			_protocol->_protocol_ptp->t1_sync_tx_time.nanoseconds = sync->originTimestamp.nanoseconds;
			issueDelayReq();
			_protocol->_protocol_ptp->Q = 0;
		}
		break;
	}
}

void IEEE1588_Handler::handleFollowUp(){

}

void IEEE1588_Handler::handleDelayReq(char * data)
{
	switch(_protocol->_protocol_ptp->_state)
	{
	case IEEE_1588::PTP_MASTER:{
		issueDelayResp();
		break;
	}
	case IEEE_1588::PTP_LISTENING:
		req = (IEEE1588_Msg::MsgDelayReq*)data;
		if(req->head.transportSpecificAndMessageType == 11){
			_protocol->_protocol_ptp->t2_sync_rx_time.seconds = req->originTimestamp.seconds;
			_protocol->_protocol_ptp->t2_sync_rx_time.nanoseconds = req->originTimestamp.nanoseconds;
		}
		if(req->head.transportSpecificAndMessageType == 1){
			_protocol->_protocol_ptp->t3_delay_req_tx_time.seconds = req->originTimestamp.seconds;
			_protocol->_protocol_ptp->t3_delay_req_tx_time.nanoseconds = req->originTimestamp.nanoseconds;
		}
		break;
	case IEEE_1588::PTP_SLAVE:
		break;
	default:
		kout << "handleDelayReq: wrong baby\n";
		return;
	}
}

void IEEE1588_Handler::handleDelayResp(char * data)
{
	resp = (IEEE1588_Msg::MsgDelayResp*)data;
	_protocol->_protocol_ptp->t4_delay_req_rx_time.seconds = resp->receiveTimestamp.seconds;
	_protocol->_protocol_ptp->t4_delay_req_rx_time.nanoseconds = resp->receiveTimestamp.nanoseconds;
	_servo->calculateOffset(_protocol->_protocol_ptp);
}

void IEEE1588_Handler::handleAnnounce()
{
	IEEE1588_Msg::MsgAnnounce announce;
	u16   sequence_delta;
	switch(_protocol->_protocol_ptp->_state)
	{
	case IEEE_1588::PTP_FAULTY:
	case IEEE_1588::PTP_INITIALIZING:
	case IEEE_1588::PTP_DISABLED:
		handler_out << "handleAnnounce: FAULTY, INITIALIZING or DISABLED, disregard\n";
		return;
	case IEEE_1588::PTP_UNCALIBRATED:
	case IEEE_1588::PTP_SLAVE:
		handler_out << "handleAnnounce: SLAVE or UNCALIBRATED:\n";
		_protocol->_protocol_ptp->record_update = true;
		_protocol->_protocol_ptp->parent_last_announce_sequence_number = announce.head.sequenceId;
		break;
	case IEEE_1588::PTP_MASTER:
	default:
		_protocol->_protocol_ptp->record_update = true;
		handler_out << "handleAnnounce: call add foreign\n";
		break;
	}
}

/* ISSUE Methods*/

void IEEE1588_Handler::issueSync()
{
	int ret;
	++_protocol->_protocol_ptp->last_sync_tx_sequence_number;
	_protocol->_protocol_ptp->grandmaster_sequence_number = _ptp->last_sync_tx_sequence_number;
	_protocol->_protocol_ptp->sentSync= true;
	char * msg;
	msg = (char *)&(_msg->_sync);
	_protocol->_protocol_ptp->t1_sync_tx_time.seconds = _rtc.getTimeStamp();
	kout << "RTC " << _rtc.getTimeStamp() <<"\n";
	_msg->msgPackSync(_protocol->_protocol_ptp, _time);
	ret = _nic->send(NIC::BROADCAST, (NIC::Protocol) 1, msg, sizeof(IEEE1588_Msg::MsgSync));
	_ptp->msgObuf ="";
	free(msg);
}

void IEEE1588_Handler::issueAnnounce(){
	IEEE1588_Msg::TimeInternal         internalTime;
	IEEE1588_Msg::TimeRepresentation originTimestamp;
	int ret;
	int packet_length;
	char * msg;
	++_protocol->_protocol_ptp->last_announce_tx_sequence_number;
	_protocol->_protocol_ptp->grandmaster_sequence_number = _protocol->_protocol_ptp->last_announce_tx_sequence_number;
	msg = (char *)&(_msg->_announce);
	_msg->msgPackAnnounce(_protocol->_protocol_ptp);
	for(int i=0;i<2;i++){
		ret =	_nic->send(NIC::BROADCAST, (NIC::Protocol) 1, msg, sizeof(IEEE1588_Msg::MsgAnnounce));
		_msg->msgPackAnnounce(_protocol->_protocol_ptp);
	}
	free(msg);
}

void IEEE1588_Handler::issueDelayResp(){
	int ret;
	++_protocol->_protocol_ptp->last_general_event_sequence_number;
	char * msg;
	msg = (char *)&(_msg->_delay_resp);
	_msg->msgPackDelayResp(_protocol->_protocol_ptp, _time);
	for(int i=0;i<2;i++){
		ret = _nic->send(NIC::BROADCAST, (NIC::Protocol) 1, msg, sizeof(IEEE1588_Msg::MsgDelayResp));
		_msg->msgPackDelayResp(_protocol->_protocol_ptp, _time);
		//		kout << "Delay Resp" << ret << "\n";
	}
	free(msg);
}

void IEEE1588_Handler::issueFollowup(IEEE1588_Msg::TimeInternal * _time){
}

void IEEE1588_Handler::issueDelayReq()
{
	int	ret;
	_protocol->_protocol_ptp->sentDelayReq = true;
	_protocol->_protocol_ptp->sentDelayReqSequenceId = ++_protocol->_protocol_ptp->last_delay_req_tx_sequence_number;
	ret = 0;
	char * msg;
	msg = (char *)&(_msg->_delay_req);
	_msg->msgPackDelayReq_Aux(_protocol->_protocol_ptp, _time);
	for(int i=0;i<2;i++){
		ret =	_nic->send(NIC::BROADCAST, (NIC::Protocol) 1, msg, sizeof(IEEE1588_Msg::MsgDelayReq));
		_msg->msgPackDelayReq_Aux(_protocol->_protocol_ptp, _time);
	}
	_protocol->_protocol_ptp->t3_delay_req_tx_time.seconds = _rtc.getTimeStamp();
	_protocol->_protocol_ptp->t3_delay_req_tx_time.nanoseconds = (_time->time_stamp()* 1000000 / _time->frequency());
	_msg->msgPackDelayReq(_protocol->_protocol_ptp, _time);
	for(int i=0;i<2;i++){
		ret =	_nic->send(NIC::BROADCAST, (NIC::Protocol) 1, msg, sizeof(IEEE1588_Msg::MsgDelayReq));
	}
	free(msg);
}

__END_SYS
