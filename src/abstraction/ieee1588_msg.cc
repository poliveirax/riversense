// EPOS IEEE 1588 Message Implementation

// This work is licensed under the EPOS Software License v1.0.
// A copy of this license is available at the EPOS system source tree root.
// A copy of this license is also available online at:
// http://epos.lisha.ufsc.br/EPOS+Software+License+v1.0
// Note that EPOS Software License applies to both source code and executables.

#include <ieee1588_msg.h>
#include <ieee1588.h>
#include <ieee1588_ptp.h>
#include <utility/ostream.h>

__BEGIN_SYS

// Class attributes
OStream mout;

// This_IEEE1588 Message class attributes

// Methods

IEEE1588_Msg::IEEE1588_Msg()
{
}

IEEE1588_Msg::~IEEE1588_Msg()
{
}

u8 IEEE1588_Msg::msgGetPtpVersion (char * buf)
{
	return ( *(((u8*)buf)+1) & 0x0F);
}


void IEEE1588_Msg::msgUnpackHeader(char *buf)
{
}

void IEEE1588_Msg::msgUnpackSync(char *buf)
{
}

void IEEE1588_Msg::msgUnpackFollowUp(char * buf, IEEE1588_Msg::MsgFollowUp *follow){
}

void IEEE1588_Msg::msgUnpackAnnounce(char *buf)
{
}

void IEEE1588_Msg::msgUnpackDelayResp(char *buf, IEEE1588_Msg::MsgDelayResp   *resp )
{

}

void IEEE1588_Msg::msgUnpackDelayReq(char *buf, IEEE1588_Msg::MsgDelayResp   *resp )
{
}



// PACK MESSAGES

void IEEE1588_Msg::msgPackHeader(IEEE1588_Msg::Header * header, IEEE1588_PTP * _ptp){
	header->transportSpecificAndMessageType = 0;
	header->reserved1AndVersionPTP = _ptp->version_number;
	header->messageLength = 0;
	header->domainNumber = _ptp->domain_number;
	header->sequenceId = 0;
	header->control = 0;
}

void IEEE1588_Msg::msgPackSync(IEEE1588_PTP * ptp, ARM7_TSC * time)
{
	/* PTP Header */
	msgPackHeader(&(_sync.head), ptp);
	/* Message type */
	_sync.head.transportSpecificAndMessageType  = 5;
	_sync.head.messageLength =  SYNC_LENGTH;
	_sync.head.sequenceId = ptp->last_sync_tx_sequence_number;
	_sync.head.sourcePortIdentity = 0;
	//mout << "2 id : " << _sync.head.sequenceId << "\n";

	/* Timestamp: */
	_sync.originTimestamp.epoch_number =  ptp->epoch_number;
	//_sync.originTimestamp.seconds =  ptp->t1_sync_tx_time.seconds;
	_sync.originTimestamp.seconds = ptp->t1_sync_tx_time.seconds;
	_sync.originTimestamp.nanoseconds =  time->time_stamp()* 1000000 / time->frequency();
}

void IEEE1588_Msg::msgPackAnnounce(IEEE1588_PTP * _ptp){

	mout << "msgPackAnnounce:\n";
	/* PTP Header */
	msgPackHeader(&(_announce.head), _ptp);
	/* Message type */
	_announce.head.transportSpecificAndMessageType  &= 0xF0;
	/* Clear previous Message type */
	_announce.head.transportSpecificAndMessageType = ANNOUNCE_MESSAGE;
	/* Length */
	_announce.head.messageLength =  sizeof(MsgAnnounce); /* Length */
}

void IEEE1588_Msg::msgPackDelayReq(IEEE1588_PTP * ptp,ARM7_TSC * time)
{
	msgPackHeader(&(_delay_req.head), ptp);
	_delay_req.head.transportSpecificAndMessageType = 1;
	/* Timestamp: */
	_delay_req.originTimestamp.epoch_number =  ptp->epoch_number;
	_delay_req.originTimestamp.seconds = ptp->t3_delay_req_tx_time.seconds;
	_delay_req.originTimestamp.nanoseconds =  ptp->t3_delay_req_tx_time.nanoseconds;

}


void IEEE1588_Msg::msgPackDelayReq_Aux(IEEE1588_PTP * ptp,ARM7_TSC * time)
{
	msgPackHeader(&(_delay_req.head), ptp);
	_delay_req.head.transportSpecificAndMessageType = 11;
	/* Timestamp: */
	_delay_req.originTimestamp.epoch_number =  ptp->epoch_number;
	_delay_req.originTimestamp.seconds = ptp->t2_sync_rx_time.seconds;
	_delay_req.originTimestamp.nanoseconds =  ptp->t2_sync_rx_time.nanoseconds;

}



void IEEE1588_Msg::msgPackDelayResp(IEEE1588_PTP * ptp, ARM7_TSC * time)
{
	/* PTP Header */
	msgPackHeader(&(_delay_resp.head), ptp);
	/* Message type, length, flags, Sequence, Control, log mean message interval */
	_delay_resp.head.transportSpecificAndMessageType = 9;
	//_delay_resp.head.domainNumber  =  ptp->domain_number;
	/* Timestamp */
	_delay_resp.receiveTimestamp.epoch_number =  ptp->epochNumber;
	_delay_resp.receiveTimestamp.seconds =  ptp->t4_delay_req_rx_time.seconds;
	_delay_resp.receiveTimestamp.nanoseconds =  ptp->t4_delay_req_rx_time.nanoseconds;
	/* requestingPortId, copy from Delay Request header */
}


void IEEE1588_Msg::msgPackFollowUp(IEEE1588_PTP * ptp )
{
}


__END_SYS
