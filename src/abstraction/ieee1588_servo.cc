// EPOS IEEE 1588 Servo Implementation

// This work is licensed under the EPOS Software License v1.0.
// A copy of this license is available at the EPOS system source tree root.
// A copy of this license is also available online at:
// http://epos.lisha.ufsc.br/EPOS+Software+License+v1.0
// Note that EPOS Software License applies to both source code and executables.

#include <ieee1588_bmc.h>
#include <ieee1588_servo.h>
#include <ieee1588_msg.h>
#include <ieee1588.h>
#include <ieee1588_ptp.h>
#include <utility/ostream.h>
#include <clock.h>
__BEGIN_SYS

// Class attributes
OStream sout;

// This_IEEE1588 Servo class attributes

// Methods

IEEE1588_Servo::IEEE1588_Servo()
{
}

IEEE1588_Servo::~IEEE1588_Servo()
{
}

void IEEE1588_Servo::setDate(){
}

unsigned long IEEE1588_Servo::getSeconds(){
	return rtc.getTimeStamp();
}

unsigned long IEEE1588_Servo::getMicro(){
	return rtc.getMicro();
}

void IEEE1588_Servo::setTimer(ARM7_TSC * t){
	_timer = t;
}

void IEEE1588_Servo::initClockVars(IEEE1588_PTP * _ptp){
	_ptp->master_to_slave_delay.nanoseconds = 0;
	_ptp->master_to_slave_delay.seconds = 0;

	_ptp->slave_to_master_delay.nanoseconds = 0;
	_ptp->slave_to_master_delay.seconds = 0;

	_ptp->offset_from_master.nanoseconds = 0;
	_ptp->offset_from_master.seconds = 0;

	_ptp->outboundLatency.nanoseconds = 0;
	_ptp->outboundLatency.seconds = 0;

	_ptp->inboundLatency.nanoseconds = 0;
	_ptp->inboundLatency.seconds = 0;

	_ptp->ofm_filter.y                        = 0;
	_ptp->ofm_filter.nsec_prev                = -1;

	_ptp->observed_drift       = 0;
	_ptp->owd_filter.s_exp       = 0;

}

void IEEE1588_Servo::initTimer(int seconds, u32 microseconds){

}


void IEEE1588_Servo::calculateOffset(IEEE1588_PTP * ptp){
	long long offset, offsetmicro;
	offset = 0;offsetmicro = 0;

	sout << " t1 "<<ptp->t1_sync_tx_time.seconds <<"\n";
	sout << " t2 "<<ptp->t2_sync_rx_time.seconds <<"\n";
	sout << " t3 "<< ptp->t3_delay_req_tx_time.seconds <<"\n";
	sout << " t4 "<<ptp->t4_delay_req_rx_time.seconds <<"\n";

	offset = (((long long)ptp->t2_sync_rx_time.seconds - (long long)ptp->t1_sync_tx_time.seconds) - ((long long)ptp->t4_delay_req_rx_time.seconds - (long long)ptp->t3_delay_req_tx_time.seconds))/2;
	offsetmicro = ((ptp->t2_sync_rx_time.nanoseconds - ptp->t1_sync_tx_time.nanoseconds) - ( ptp->t4_delay_req_rx_time.nanoseconds - ptp->t3_delay_req_tx_time.nanoseconds))/2;
	offsetmicro -= rtc.getMicro();
	sout << "OFFSEt Sec : "<< offset << "\n";
	sout << "OFFSEt Micro : "<< offsetmicro << "\n";

	if(offset < 0)
		offset = (long long)rtc.getTimeStamp() - offset;
	else
		offset = (long long)rtc.getTimeStamp() - offset;

	rtc.setMicro((long)offsetmicro);
	if((offset < 1559140643) && (offset > -1559140643))
		rtc.setTimeStamp((unsigned long)offset);

	//DEBUG
	sout << "RTC Micro "<< rtc.getMicro() << "\n";
	sout << "RTC Sec "<< rtc.getTimeStamp() << "\n";


}

void IEEE1588_Servo::calculaDelay(IEEE1588_PTP * ptp){
	IEEE1588_PTP::TimeInternal offset;
	offset.seconds = ((ptp->t2_sync_rx_time.seconds - ptp->t1_sync_tx_time.seconds) + (ptp->t4_delay_req_rx_time.seconds - ptp->t3_delay_req_tx_time.seconds))/2;
	ptp->master_to_slave_delay.seconds = offset.seconds;
}

void IEEE1588_Servo::adjustTime(IEEE1588_PTP * ptp){


}

__END_SYS

